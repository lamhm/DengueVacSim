# List of the variables used in the model. These variables are fixed during the
# simulations and not affected by the model fitting.
vars:
    # The size of each age group, defined by timespans (in days).
    # Type: Vector{Float}
    ageGroupSizes: [
        # 1      2      3      4      5      6
        728.0,        728.0,        728.0,        # 00 - 06
        364.0, 364.0, 364.0, 364.0, 728.0,        # 07 - 12
        728.0,        728.0,                      # 13 - 16
        16016.0]                                  # 17 - 60

    nDaysPerYear: 364.0
    nDaysPerSummaryPeriod: 14
    nDaysReportingLag: 0.0

    nSimYears: 20.0

    # The simulated year that the vaccination start.
    # The first simulated year is indexed as zero.
    # If this value is set to negative, vaccination will be disable.
    simYearToStartVaccination: 0.0

    # The following flag indicates whether immigrants originate from a completely
    # unvaccinated population or not.
    noVacBeforeImmigration: true


    # The initial population size (based on HCMC census data)
    # initPopSize: 6291055.0  # data of 2005
    # initPopSize: 8247829.0  # data of 2015
    initPopSize: 9038600.0  # GSO data of 2019

    # The natural population growth rate of HCMC, taken from population
    # censuses. Type: Vector{Float}
    # annualNatPopGrowthRates: [
    #     1.01150, 1.01075, 1.01096, 1.01029, 1.01037,
    #     1.01035, 1.00979, 1.01007, 1.00908, 1.00826, 1.00817]
    # annualNatPopGrowthRates: [
    #     1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
    #     1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0
    # ]
    annualNatPopGrowthRates: [
        1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01,
        1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01,
        1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01,
        1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01,
        1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01,
        1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01,
        1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01,
        1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01,
        1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01,
        1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01, 1.01
    ]

    # Annual net migration rate of HCMC, taken from population censuses.
    # annualNetMigRates: [
    #     0.02807, 0.02795, 0.02444, 0.02173, 0.01780,
    #     0.01627, 0.01598, 0.01607, 0.00972, 0.00827, 0.01331]
    annualNetMigRates: [
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
    ]

    # Relative monthly birth rate in HCMC, calculated based on the month of
    # births of non-infant cases at CH1 and CH2.
    seasonalBirthRates: [
        0.9723142, 0.8797572, 0.9097778, 0.9274340,
        0.9316972, 0.8917952, 0.9887550, 1.0506416,
        1.1184925, 1.1752437, 1.1195492, 1.0345424]

    # Number of circulating dengue serotypes.
    nSerotypes: 4

    # Number of seasonal periods (per year) for transmission
    nSeasonalTranPeriods: 3

    # Age cut-offs for age-specific hospitalisation rates
    hospRateCutoffs: [364 * 2.0, 364 * 1000.0]

    # The next 3 variables are used for the ODE solver
    nOdeWarmupYears: 19
    odeAbsTolerance: 1e-9  # Default: 1e-9
    odeRelTolerance: 1e-5  # Default: 1e-5





# List of the parameters of the model. Technically, these values can be altered
# during the model fitting process. Therefore, the values specified here will
# only be used for the initial state of the model.
params:
    # Serotype-specific transmission rate.
    serotypeTranRates: [0.399, 0.399, 0.399, 0.399]

    # The seasonal scale of the transmission relatively to the 1st period.
    # The 1st seasonal transmission scale must always be 1.0.
    seasonalTranScales: [1.00, 1.20, 1.11]

    recoveryRate: 1/7

    # The average duration of cross-immunity induced by a primary infection
    # Research on Dengue During World War II suggested a duration of 60-120 days.
    crossProtectDuration: 611.0

    # The risk of infection during the primary cross-protection period.
    # This parameter ranges between 0 and 1.
    infectRiskDuringPriCpr: 1.0

    hospRates: [0.34, 0.17]

    # The effect size of ADE in increasing the risk of hospitalisation
    # in infants
    adeEffectScale: 15.8

    # The average age at which ADE occurs in infants.
    adeAgeAvg: 246.0  # 8 months

    # The variance of the ages of the infants who are at risk of ADE.
    adeAgeVar: 3441.0

    # The duration of the protective effect induced by maternal immunity
    # 191229: This is fixed at 0.0 as the number of infections of <6 months old
    #         is too low to estimate this parameter.
    maternalProtectDuration: 100.0

    sndSymptomaticProb: 0.41  # from Hannah's paper (2017)
    priSymptomaticProb: 0.01

    # The coverage of screening for each age group
    screeningCoverages: [
        # 1    2    3    4    5    6
        0.0,      0.0,      0.0,       # 00 - 06
        0.0, 0.0, 0.7, 0.0, 0.0,       # 07 - 12
        0.0,      0.0,                 # 13 - 16
        0.0 ]                          # 17 - 60
    # screeningCoverages: [
    #     # 1    2    3    4    5    6
    #     0.5,      0.5,      0.5,       # 00 - 06
    #     0.5, 0.5, 0.5, 0.5, 0.5,       # 07 - 12
    #     0.5,      0.5,                 # 13 - 16
    #     0.5 ]                          # 17 - 60

    # Vaccine immune response rate: This is the probability that a vaccine
    # recipient develop immune response (which can be either protective or
    # harmful) to the vaccine.
    vaccineResponseProb: 0.7

    # Probability that the vaccine creates a transmission-blocking effect on
    # effectively vaccinated recipients during their secondary infections.
    transBlockingEfficacy: 1.0

    negVaccinatedProb: 1.0
    priVaccinatedProb: 1.0
    sndVaccinatedProb: 1.0
