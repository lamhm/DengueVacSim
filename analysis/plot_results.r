## *****************************************************************************
## Clean the environment, load libraries and prepare plots  ----
## *****************************************************************************
{
    rm(list = ls())

    require(ggplot2)
    require(RUtil)

    ## Load fonts for PDF
    require(extrafont)
    extrafont::loadfonts(device = "pdf", quiet = T)

    ## Set plot theme
    ggplot2::theme_set(
        ggplot2::theme_gray(base_family = "Oswald", base_size = 12)
    )
    ggplot2::theme_update(
        plot.title = element_text(size = rel(1.1), face = "bold"),
        axis.text = element_text(size = rel(0.9)),
        axis.title = element_text(size = rel(1)),
        panel.grid.major = element_line(color = "#cccccc", size = 0.3),
        panel.grid.minor = element_blank(),
        panel.background = element_rect(colour = "#777777", fill = "#f8f8f8", size = 1),
        legend.title = element_text(size = rel(1)),
        legend.text = element_text(size = rel(0.9)),
        legend.spacing = unit(5, "pt"),
        legend.key.height = unit(1, "pt"),
        legend.key = element_blank(),
        legend.box = "vertical",
        legend.position = c(0.0, 1.08),
        legend.justification = c("left", "bottom"),
        legend.direction = "horizontal",
        legend.margin = ggplot2::margin(0, 0, 0, 0, "pt"),
        plot.margin = ggplot2::margin(t = 1.0, r = 0.1, b = 0.1, l = 0.1, "cm"),
        strip.background = element_rect(colour = NA, fill = "#d0d0d0"),
        strip.text = element_text(size = rel(1))
    )

    noVacColor <- "#000000"
    colorList  <- c("#BE2B4E", "#3CAFBA", "#0B48BB")
    dodgeWidth <- 0.7


    ggTiledPlot <- function(xmax, data = NA) {
        tiles <- data.frame( xmin = seq(0.5, xmax, 2),
                             xmax = seq(1.5, xmax + 0.5, 2) )

        newPlot <- ggplot( data = data ) +
            geom_rect( data = tiles, aes(xmin = xmin, xmax = xmax),
                       ymin = -Inf, ymax = Inf, fill = "#0000000D" ) +
            expand_limits( x = xmax + 0.5 ) +
            scale_y_continuous( expand = c(0.1, 0.1) ) +
            theme( panel.grid.major.x = element_blank() )

        if (xmax <= 9) {
            newPlot <- newPlot +
                scale_x_discrete( expand = c(0, 0) )
        } else {
            newPlot <- newPlot +
                scale_x_discrete( breaks = seq(1, xmax, by = xmax/10),
                                  expand = c(0, 0) )
        }

        return(newPlot)
    }


    to_scientific <- function(x, digits = 1) {
        return( sub("e\\+0", "e", formatC(x, format = "e", digits = 1)) )
    }
}


## *****************************************************************************
## Load data  ----
## *****************************************************************************
homeDir <- "/home/lamhm/Workspace/DengueVacSim/"

setwd(homeDir %_% "analysis/complete_transmission_blocking/vaccinate_via_aging")
# setwd(homeDir %_% "analysis/no_transmission_blocking/vaccinate_via_aging")

# setwd(homeDir %_% "analysis/complete_transmission_blocking/vaccinate_within_age_groups")
# setwd(homeDir %_% "analysis/no_transmission_blocking/vaccinate_within_age_groups")

# setwd(homeDir %_% "analysis/complete_transmission_blocking/100_years")
# setwd(homeDir %_% "analysis/no_transmission_blocking/100_years")

# setwd(homeDir %_% "test/")


simResultsFileName <- "sim_results_1000"
simResults <- read.csv(simResultsFileName %_% ".csv", stringsAsFactors = F)


## Initialise the plot storage
plots <- list()


## *****************************************************************************
## Format data  ----
## *****************************************************************************
{
    simResults$screeningCoverage <-
        paste0(simResults$screeningCoverage * 100, "% Coverage")
    simResults$vaccineResponseProb <-
        paste0(simResults$vaccineResponseProb * 100, "% Immuno-effectiveness")

    percentageColNames <- c(
        "avtHosp", "cumAvtHosp", "vacInducedHospPerc",
        "percNegVaccines", "percPriVaccines", "percSndVaccines"
    )
    for (colName in percentageColNames) {
        colIdx <- startsWith(names(simResults), colName)
        simResults[, colIdx] <- simResults[, colIdx] * 100
    }
    rm(colName, percentageColNames, colIdx)

    simResults$scenario <- factor(
        simResults$screeningScenario,
        levels = c("no vaccine",
                   "no screening",
                   "realistic screening",
                   "perfect screening"),
        labels = c("No vaccine",
                   "Vaccine without screening",
                   "Vaccine with realistic screening",
                   "Vaccine with perfect screening"),
        ordered = T
    )
    simResults$screeningStrategy <- factor(
        simResults$screeningScenario,
        levels = c("no vaccine",
                   "no screening",
                   "realistic screening",
                   "perfect screening"),
        labels = c("No vaccine",
                   "No screening",
                   "Realistic screening",
                   "Perfect screening"),
        ordered = T
    )

    simResults$time <- round(simResults$time)
    simResults$year <- factor(
        simResults$time, levels = 1:20,
        labels = c("1st", "2nd", "3rd", paste0(4:20, "th"))
    )
    simResults$time <- as.factor(simResults$time)

    fullResults <- simResults
    simResults <-
        simResults[which(simResults$time %in% c(1, 2, 3, 5, 10, 15, 20)), ]

    vaccineSimResults <-
        simResults[which(simResults$scenario != "No vaccine"), ]
    fullVaccineResults <-
        fullResults[which(fullResults$scenario != "No vaccine"), ]

    noVacResults <- fullResults[which(
        fullResults$scenario == "No vaccine"
        & fullResults$screeningCoverage == min(fullResults$screeningCoverage)
        & fullResults$vaccineResponseProb == min(fullResults$vaccineResponseProb))
        , ]

    nTimes_long <- length(unique(fullVaccineResults$time))
    nTimes_short <- length(unique(simResults$time))
}


## *****************************************************************************
## Plot the total hospitalisation counts  ----
## *****************************************************************************
plots$totalHospCounts.noVac <- ggplot(data = noVacResults) +
    geom_line( aes(x = time, y = totalHospCounts_500),
               group = 1, color = noVacColor, size = 0.8 ) +
    geom_ribbon( aes(x = time,
                     ymin = totalHospCounts_025, ymax = totalHospCounts_975),
                 fill = noVacColor, alpha = 0.15, color = "grey",
                 group = 1 ) +
    scale_y_continuous( labels = to_scientific ) +
    expand_limits( y = 0 ) +
    scale_x_discrete( breaks = seq(1, nTimes_long, by = nTimes_long/10) ) +
    labs( x = "Year",
          y = "Number of Symptomatic Non-infant Cases Per Year (without vaccine deployment)" )

print(plots$totalHospCounts.noVac)


plots$totalHospCounts <- ggTiledPlot( xmax = nTimes_short, data = simResults ) +
    geom_pointrange( aes(x = year, y = totalHospCounts_500,
                         ymin = totalHospCounts_025, ymax = totalHospCounts_975,
                         group = scenario, colour = scenario),
                     position = position_dodge(width = 0.8),
                     size = 0.9, fatten = 1.5 ) +
    scale_color_manual( values = c(noVacColor, colorList) ) +
    scale_y_continuous( expand = c(0.1, 0.1), labels = to_scientific ) +
    expand_limits( y = c(0, 1e5) ) +
    labs( x = "Year of Vaccination Programme",
          y = "Number of Symptomatic Non-infant Cases Per Year",
          colour = "Scenario") +
    facet_grid( screeningCoverage ~ vaccineResponseProb,
                scales = "fixed", as.table = F )

print(plots$totalHospCounts)


plots$totalHospCounts_full <-
    ggTiledPlot( xmax = nTimes_long, data = fullResults ) +
    geom_linerange( aes(x = time,
                        ymin = totalHospCounts_025, ymax = totalHospCounts_975,
                        group = scenario, colour = scenario),
                     position = position_dodge(width = 0.8),
                     size = 0.7 ) +
    scale_color_manual( values = c(noVacColor, colorList) ) +
    scale_y_continuous( expand = c(0.1, 0.1), labels = to_scientific ) +
    expand_limits( y = c(0, 1e5) ) +
    labs( x = "Year of Vaccination Programme",
          y = "Number of Symptomatic Non-infant Cases Per Year",
          colour = "Scenario") +
    facet_grid( screeningCoverage ~ vaccineResponseProb,
                scales = "fixed", as.table = F )

print(plots$totalHospCounts_full)


## *****************************************************************************
## Plot the averted hospitalisations  ----
## *****************************************************************************
plots$avtHosp <- ggTiledPlot( xmax = nTimes_short, data = vaccineSimResults ) +
    geom_pointrange( aes(x = year, y = avtHosp_500,
                         ymin = avtHosp_025, ymax = avtHosp_975,
                         group = screeningStrategy, colour = screeningStrategy),
                     position = position_dodge(width = dodgeWidth),
                     size = 0.9, fatten = 1.5 ) +
    scale_color_manual( values = colorList ) +
    expand_limits( y = 0 ) +
    labs( x = "Year of Vaccination Programme",
          y = "Percentage of Non-infant Hospitalisations Averted Per Year",
          colour = "Screening Strategy") +
    facet_grid( screeningCoverage ~ vaccineResponseProb,
                scales = "fixed", as.table = F )

print(plots$avtHosp)


plots$avtHosp_full <- ggTiledPlot( xmax = nTimes_long, data = fullVaccineResults ) +
    geom_linerange( aes(x = time,
                        ymin = avtHosp_025, ymax = avtHosp_975,
                        group = screeningStrategy, colour = screeningStrategy),
                    position = position_dodge(width = dodgeWidth),
                    size = 0.8 ) +
    scale_colour_manual( values = colorList ) +
    expand_limits( y = 0 ) +
    labs( x = "Year of Vaccination Programme",
          y = "Percentage of Non-infant Hospitalisations Averted Per Year",
          colour = "Screening Strategy") +
    facet_grid( screeningCoverage ~ vaccineResponseProb,
                scales = "fixed", as.table = F )

print(plots$avtHosp_full)



## *****************************************************************************
## Plot the cumulative averted hospitalisations  ----
## *****************************************************************************
plots$cumAvtHosp <- ggTiledPlot( xmax = nTimes_short, data = vaccineSimResults ) +
    geom_pointrange( aes(x = year, y = cumAvtHosp_500,
                         ymin = cumAvtHosp_025, ymax = cumAvtHosp_975,
                         group = screeningStrategy, colour = screeningStrategy),
                     position = position_dodge(width = dodgeWidth),
                     size = 0.9, fatten = 1.5 ) +
    scale_color_manual( values = colorList ) +
    expand_limits( y = 0 ) +
    labs( x = "Year of Vaccination Programme",
          y = "Averted Non-infant Hospitalisations Since the Start of Vaccination (%)",
          colour = "Screening Strategy") +
    facet_grid( screeningCoverage ~ vaccineResponseProb,
                scales = "fixed", as.table = F )

print(plots$cumAvtHosp)


plots$cumAvtHosp_full <-
    ggTiledPlot( xmax = nTimes_long, data = fullVaccineResults ) +
    geom_linerange( aes(x = time,
                        ymin = cumAvtHosp_025, ymax = cumAvtHosp_975,
                        group = screeningStrategy, colour = screeningStrategy),
                    position = position_dodge(width = dodgeWidth),
                    size = 0.8 ) +
    scale_colour_manual( values = colorList ) +
    expand_limits( y = 0 ) +
    labs( x = "Year of Vaccination Programme",
          y = "Averted Non-infant Hospitalisations Since the Start of Vaccination (%)",
          colour = "Screening Strategy") +
    facet_grid( screeningCoverage ~ vaccineResponseProb,
                scales = "fixed", as.table = F )


print(plots$cumAvtHosp_full)


## *****************************************************************************
## Plot the proportion hospitalisations caused by false vaccinations  ----
## *****************************************************************************
plots$vacInducedHospPerc <-
    ggTiledPlot( xmax = nTimes_short, data = vaccineSimResults ) +
    geom_pointrange( aes(x = year, y = vacInducedHospPerc_500,
                         ymin = vacInducedHospPerc_025,
                         ymax = vacInducedHospPerc_975,
                         group = screeningStrategy, colour = screeningStrategy),
                     position = position_dodge(width = dodgeWidth),
                     size = 0.9, fatten = 1.5 ) +
    scale_color_manual( values = colorList ) +
    expand_limits( y = c(0, 50) ) +
    labs( x = "Year of Vaccination Programme",
          y = "Annual Percentage of Non-infant Hospitalisations " %_%
              "Being Naïve Vaccinees",
          colour = "Screening Strategy") +
    facet_grid( screeningCoverage ~ vaccineResponseProb,
                scales = "fixed", as.table = F )

print(plots$vacInducedHospPerc)


plots$vacInducedHospPerc_full <-
    ggplot( data = fullVaccineResults ) +
    geom_line( aes(x = time,
                   y = vacInducedHospPerc_500,
                   group = screeningStrategy, colour = screeningStrategy) ) +
    geom_ribbon( aes(x = time,
                     ymin = vacInducedHospPerc_025,
                     ymax = vacInducedHospPerc_975,
                     group = screeningStrategy, fill = screeningStrategy),
                 alpha = 0.3 ) +
    scale_color_manual( values = colorList ) +
    scale_fill_manual( values = colorList ) +
    scale_x_discrete( breaks = seq(1, nTimes_long, by = nTimes_long/10) ) +
    expand_limits( y = 0 ) +
    guides( fill = guide_none() ) +
    labs( x = "Year of Vaccination Programme",
          y = "Annual Percentage of Non-infant Hospitalisations " %_%
              "Being Naïve Vaccinees",
          colour = "Screening Strategy" ) +
    facet_grid( screeningCoverage ~ vaccineResponseProb,
                scales = "fixed", as.table = F )

print(plots$vacInducedHospPerc_full)


## *****************************************************************************
## Plot the annual number of hospitalisations caused by false vaccinations  ----
## *****************************************************************************
plots$vacInducedHospCount <-
    ggTiledPlot( xmax = nTimes_short, data = vaccineSimResults ) +
    geom_pointrange( aes(x = year, y = vacInducedHospCount_500,
                         ymin = vacInducedHospCount_025,
                         ymax = vacInducedHospCount_975,
                         group = screeningStrategy, colour = screeningStrategy),
                     position = position_dodge(width = dodgeWidth),
                     size = 0.9, fatten = 1.5 ) +
    scale_color_manual( values = colorList ) +
    expand_limits( y = 0 ) +
    scale_y_continuous( labels = to_scientific ) +
    labs( x = "Year of Vaccination Programme",
          y = "Annual Number of Naïve Vaccinees Experiencing " %_%
              "a Symptomatic Primary Infection",
          colour = "Screening Strategy") +
    facet_grid( screeningCoverage ~ vaccineResponseProb,
                scales = "fixed", as.table = F )

print(plots$vacInducedHospCount)


plots$vacInducedHospCount_full <-
    ggplot( data = fullVaccineResults ) +
    geom_line( aes(x = time,
                   y = vacInducedHospCount_500,
                   group = screeningStrategy, colour = screeningStrategy) ) +
    geom_ribbon( aes(x = time,
                     ymin = vacInducedHospCount_025,
                     ymax = vacInducedHospCount_975,
                     group = screeningStrategy, fill = screeningStrategy),
                 alpha = 0.3 ) +
    scale_color_manual( values = colorList ) +
    scale_fill_manual( values = colorList ) +
    scale_x_discrete( breaks = seq(1, nTimes_long, by = nTimes_long/10) ) +
    expand_limits( y = 0 ) +
    scale_y_continuous( labels = to_scientific ) +
    guides( fill = guide_none() ) +
    labs( x = "Year of Vaccination Programme",
          y = "Annual Number of Naïve Vaccinees Experiencing " %_%
              "a Symptomatic Primary Infection",
          colour = "Screening Strategy" ) +
    facet_grid( screeningCoverage ~ vaccineResponseProb,
                scales = "fixed", as.table = F )

print(plots$vacInducedHospCount_full)


## *****************************************************************************
## Plot cumulative number of vaccine shots  ----
## *****************************************************************************
plots$nVaccines <-
    ggTiledPlot( xmax = nTimes_short, data = vaccineSimResults ) +
    geom_pointrange( aes(x = year,
                         y = nVaccines_500,
                         ymin = nVaccines_025,
                         ymax = nVaccines_975,
                         group = screeningStrategy, colour = screeningStrategy),
                     position = position_dodge(width = dodgeWidth),
                     size = 0.9, fatten = 1.5 ) +
    scale_color_manual( values = colorList ) +
    expand_limits( y = 0 ) +
    scale_y_continuous( expand = c(0.1, 0.1), labels = to_scientific ) +
    labs( x = "Year of Vaccination Programme",
          y = "Cumulative Number of Vaccines",
          colour = "Screening Strategy") +
    facet_grid( screeningCoverage ~ vaccineResponseProb,
                scales = "fixed", as.table = F )

print(plots$nVaccines)


## *****************************************************************************
## Plot annual number of vaccine shots  ----
## *****************************************************************************
plots$nVaccinesPerYear <-
    ggTiledPlot( xmax = nTimes_short, data = vaccineSimResults ) +
    geom_pointrange( aes(x = year,
                         y = nVaccinesPerYear_500,
                         ymin = nVaccinesPerYear_025,
                         ymax = nVaccinesPerYear_975,
                         group = screeningStrategy, colour = screeningStrategy),
                     position = position_dodge(width = dodgeWidth),
                     size = 0.9, fatten = 1.5 ) +
    scale_color_manual( values = colorList ) +
    expand_limits( y = 0 ) +
    scale_y_continuous( expand = c(0.1, 0.1), labels = to_scientific ) +
    labs( x = "Year of Vaccination Programme",
          y = "Annual Number of Vaccines",
          colour = "Screening Strategy") +
    facet_grid( screeningCoverage ~ vaccineResponseProb,
                scales = "fixed", as.table = F )

print(plots$nVaccinesPerYear)


## *****************************************************************************
## Plot annual percentage of vaccine shots given to naive individuals  ----
## *****************************************************************************
plots$percSeroGroupVaccinated <-
    ggTiledPlot( xmax = nTimes_short, data = vaccineSimResults ) +
    geom_pointrange( aes(x = year,
                         y = percNegVaccines_500,
                         ymin = percNegVaccines_025,
                         ymax = percNegVaccines_975,
                         group = screeningStrategy, colour = screeningStrategy,
                         shape = "Naive"),
                     position = position_dodge(width = 0.9),
                     size = 0.8, fatten = 2 ) +
    geom_pointrange( aes(x = year,
                         y = percPriVaccines_500,
                         ymin = percPriVaccines_025,
                         ymax = percPriVaccines_975,
                         group = screeningStrategy, colour = screeningStrategy,
                         shape = "Having 1 prior infections"),
                     position = position_dodge(width = 0.3),
                     size = 0.8, fatten = 2 ) +
    scale_color_manual( values = colorList ) +
    expand_limits( y = 0 ) +
    labs( x = "Year of Vaccination Programme",
          y = "Annual Percentage of Vaccines Given to Each Sero-Group",
          colour = "Screening Strategy",
          shape = "Recipients' Serostatus" ) +
    scale_shape_manual( values = c(19, 23) ) +
    facet_grid( screeningCoverage ~ vaccineResponseProb,
                scales = "fixed", as.table = F ) +
    guides( colour = guide_legend(order = 2),
            shape = guide_legend(order = 1) ) +
    theme( plot.margin = margin(t = 1.4, r = 0.1, b = 0.1, l = 0.1, "cm") )

print(plots$percSeroGroupVaccinated)



## *****************************************************************************
## Save all plots to a PDF file  ----
## *****************************************************************************
{
    pdf(simResultsFileName %_% ".pdf", width = 8.5, height = 6, useDingbats = F)
    for (i in 1 : length(plots)) {
        print(plots[[i]])
    }
    dev.off()
}





extraPlots <- list()

# subData <- fullVaccineResults
subData <- fullVaccineResults[
    which(fullVaccineResults$screeningScenario == "no screening"), ]
# subData <- fullVaccineResults[
#     which(fullVaccineResults$screeningScenario %in% c("no screening", "realistic screening")), ]

# subData <- subData[which(
#     subData$screeningCoverage %in% c("50% Coverage", "90% Coverage")
#     & subData$vaccineResponseProb %in% c("40% Immuno-effectiveness", "80% Immuno-effectiveness")
# ), ]


extraPlots$avtHospPerYear <-
    ggplot(data = subData) +
    geom_ribbon( aes(x = time,
                     ymin = avtHosp_025, ymax = avtHosp_975,
                     group = screeningStrategy, fill = screeningStrategy),
                 alpha = 0.4) +
    geom_line( aes(x = time, y = avtHosp_500,
                   group = screeningStrategy, colour = screeningStrategy),
               size = 0.8 ) +
    scale_colour_manual( values = colorList ) +
    scale_fill_manual( values = colorList ) +
    expand_limits( y = 0 ) +
    scale_y_continuous( expand = c(0.1, 0.1) ) +
    guides( fill = guide_none() ) +
    labs( x = "Year of Vaccination Programme",
          y = "% Hospitalisations Prevented Per Year",
          colour = "Screening Strategy") +
    scale_x_discrete( breaks = seq(1, 20, by = 2), expand = c(0.04, 0.04) ) +
    facet_grid( screeningCoverage ~ vaccineResponseProb,
                scales = "fixed", as.table = F )

print(extraPlots$avtHospPerYear)


hospRate <- 0.171
extraPlots$totalHospCounts.noVac <- ggplot(data = noVacResults) +
    geom_line( aes(x = time, y = totalHospCounts_500 * hospRate),
               group = 1, color = noVacColor, size = 0.8 ) +
    geom_ribbon( aes(x = time,
                     ymin = totalHospCounts_025 * hospRate,
                     ymax = totalHospCounts_975 * hospRate),
                 fill = noVacColor, alpha = 0.15, color = "grey",
                 group = 1 ) +
    scale_y_continuous( labels = to_scientific ) +
    expand_limits( y = 0 ) +
    scale_x_discrete( breaks = seq(1, nTimes_long, by = nTimes_long/10) ) +
    labs( x = "Year",
          y = "Estimated Annual Number of Hospitalisations (without vaccine deployment)" )

print(extraPlots$totalHospCounts.noVac)


{
    pdf(simResultsFileName %_% "_extra.pdf", width = 8.5, height = 6, useDingbats = F)
    for (i in 1 : length(extraPlots)) {
        print(extraPlots[[i]])
    }
    dev.off()
}


