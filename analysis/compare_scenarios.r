## *****************************************************************************
## Clean the environment, load libraries and prepare plots  ----
## *****************************************************************************
{
    rm(list = ls())

    require(ggplot2)
    require(RUtil)

    ## Load fonts for PDF
    require(extrafont)
    extrafont::loadfonts(device = "pdf", quiet = T)

    ## Set plot theme
    ggplot2::theme_set(
        ggplot2::theme_gray(base_family = "Oswald", base_size = 12)
    )
    ggplot2::theme_update(
        plot.title = element_text(size = rel(1.1), face = "bold"),
        axis.text = element_text(size = rel(0.9)),
        axis.title = element_text(size = rel(1)),
        panel.grid.major = element_line(color = "#cccccc", size = 0.3),
        panel.grid.minor = element_blank(),
        panel.background = element_rect(colour = "#777777", fill = "#f8f8f8", size = 1),
        legend.title = element_text(size = rel(1)),
        legend.text = element_text(size = rel(0.9)),
        legend.spacing = unit(5, "pt"),
        legend.key.height = unit(1, "pt"),
        legend.key = element_blank(),
        legend.box = "vertical",
        legend.position = c(0.0, 1.08),
        legend.justification = c("left", "bottom"),
        legend.direction = "horizontal",
        legend.margin = ggplot2::margin(0, 0, 0, 0, "pt"),
        plot.margin = ggplot2::margin(t = 1.0, r = 0.1, b = 0.1, l = 0.1, "cm"),
        strip.background = element_rect(colour = NA, fill = "#d0d0d0"),
        strip.text = element_text(size = rel(1))
    )

    # noVacColor <- "#000000"
    # colorList  <- c("#BE2B4E", "#3CAFBA", "#0B48BB")
    # dodgeWidth <- 0.7

    colorList  <- c("#BE2B4E", "#FD9644", "#0B48BB")


    ggTiledPlot <- function(xmax, data = NA) {
        tiles <- data.frame( xmin = seq(0.5, xmax, 2),
                             xmax = seq(1.5, xmax + 0.5, 2) )

        newPlot <- ggplot( data = data ) +
            geom_rect( data = tiles, aes(xmin = xmin, xmax = xmax),
                       ymin = -Inf, ymax = Inf, fill = "#0000000D" ) +
            expand_limits( x = xmax + 0.5 ) +
            scale_y_continuous( expand = c(0.1, 0.1) ) +
            theme( panel.grid.major.x = element_blank() )

        if (xmax <= 9) {
            newPlot <- newPlot +
                scale_x_discrete( expand = c(0, 0) )
        } else {
            newPlot <- newPlot +
                scale_x_discrete( breaks = seq(1, xmax, by = xmax/10),
                                  expand = c(0, 0) )
        }

        return(newPlot)
    }


    to_scientific <- function(x, digits = 1) {
        return( sub("e\\+0", "e", formatC(x, format = "e", digits = 1)) )
    }


    clean_results <- function(simResults) {
        simResults$screeningCoverage <-
            paste0(simResults$screeningCoverage * 100, "% Coverage")
        simResults$vaccineResponseProb <-
            paste0(simResults$vaccineResponseProb * 100, "% Immuno-effectiveness")

        percentageColNames <- c(
            "avtHosp", "cumAvtHosp", "vacInducedHospPerc",
            "percNegVaccines", "percPriVaccines", "percSndVaccines"
        )
        for (colName in percentageColNames) {
            colIdx <- startsWith(names(simResults), colName)
            simResults[, colIdx] <- simResults[, colIdx] * 100
        }
        rm(colName, percentageColNames, colIdx)

        simResults$scenario <- factor(
            simResults$screeningScenario,
            levels = c("no vaccine",
                       "no screening",
                       "realistic screening",
                       "perfect screening"),
            labels = c("No vaccine",
                       "Vaccine without screening",
                       "Vaccine with realistic screening",
                       "Vaccine with perfect screening"),
            ordered = T
        )
        simResults$screeningStrategy <- factor(
            simResults$screeningScenario,
            levels = c("no vaccine",
                       "no screening",
                       "realistic screening",
                       "perfect screening"),
            labels = c("No vaccine",
                       "No screening",
                       "Realistic screening",
                       "Perfect screening"),
            ordered = T
        )

        simResults$time <- round(simResults$time)
        simResults$time <- as.factor(simResults$time)
        return(simResults)
    }
}


## *****************************************************************************
## Load data  ----
## *****************************************************************************
setwd("/home/lamhm/Workspace/DengueVacSim/analysis")

simConfiguration <- "vaccinate_via_aging"
simResultsFileName <- "sim_results_1000"

# simConfiguration <- "100_years"
# simResultsFileName <- "sim_results_100"


{
    compTransBlock <- read.csv(
        "complete_transmission_blocking/" %_% simConfiguration %_% "/" %_%
            simResultsFileName %_% ".csv",
        stringsAsFactors = F
    )

    noTransBlock <- read.csv(
        "no_transmission_blocking/" %_% simConfiguration %_% "/" %_%
            simResultsFileName %_% ".csv",
        stringsAsFactors = F
    )

    compTransBlock$transBlocking <- "Complete transmission-blocking"
    noTransBlock$transBlocking <- "No transmission-blocking"
}


{
    all <- rbind(compTransBlock, noTransBlock)
    all <- clean_results(all)
    nTimes <- length(unique(all$time))
    noScreen <- all[which(all$screeningStrategy == "No screening"), ]
}


## Initialise the plot storage
plots <- list()


## *****************************************************************************
## Compare the annual percentage of hosp. contributed by naive vaccinees  ----
## *****************************************************************************
plots$vacInducedHospPerc <-
    ggTiledPlot( xmax = nTimes, data = noScreen ) +
    geom_pointrange( aes(x = time,
                         y = vacInducedHospPerc_500,
                         ymin = vacInducedHospPerc_025,
                         ymax = vacInducedHospPerc_975,
                         group = transBlocking,
                         colour = transBlocking),
                     position = position_dodge(width = 0.8),
                     size = 0.8, fatten = 1.0 ) +
    scale_color_manual( values = colorList ) +
    scale_fill_manual( values = colorList ) +
    scale_x_discrete( breaks = seq(1, nTimes, by = nTimes/10) ) +
    expand_limits( y = 0 ) +
    guides( fill = guide_none() ) +
    labs( x = "Year of Vaccination Programme",
          y = "Annual Percentage of Non-infant Hospitalisations " %_%
              "Being Naïve Vaccinees",
          colour = NULL ) +
    facet_grid( screeningCoverage ~ vaccineResponseProb,
                scales = "fixed", as.table = F )

print(plots$vacInducedHospPerc)


## *****************************************************************************
## Plot the annual number of hospitalisations caused by false vaccinations  ----
## *****************************************************************************
plots$vacInducedHospCount <-
    ggplot( data = noScreen ) +
    geom_line( aes(x = time,
                   y = vacInducedHospCount_500,
                   group = transBlocking, colour = transBlocking),
               size = 0.8, alpha = 0.8 ) +
    geom_ribbon( aes(x = time,
                     ymin = vacInducedHospCount_025,
                     ymax = vacInducedHospCount_975,
                     group = transBlocking, fill = transBlocking),
                 alpha = 0.4 ) +
    scale_color_manual( values = colorList ) +
    scale_fill_manual( values = colorList ) +
    scale_x_discrete( breaks = seq(1, nTimes, by = nTimes/10) ) +
    expand_limits( y = 0 ) +
    scale_y_continuous( labels = to_scientific ) +
    guides( fill = guide_none() ) +
    labs( x = "Year of Vaccination Programme",
          y = "Annual Number of Naïve Vaccinees Experiencing " %_%
              "a Symptomatic Primary Infection",
          colour = NULL ) +
    facet_grid( screeningCoverage ~ vaccineResponseProb,
                scales = "fixed", as.table = F )

print(plots$vacInducedHospCount)



## *****************************************************************************
## Save all plots to a PDF file  ----
## *****************************************************************************
{
    pdf("Compare trans. blocking scenarios_" %_% simResultsFileName %_% ".pdf",
        width = 8.5, height = 6, useDingbats = F)
    for (i in 1 : length(plots)) {
        print(plots[[i]])
    }
    dev.off()
}

