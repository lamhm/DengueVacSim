using DengueVacSim


paramFile          = "data/partialCpr_param_estimates.csv"
vacinnatedProbFile = "data/HcmCon31_Prob_titre_over_10_given_n_infections.csv"
modelConfigFile    = "analysis/no_transmission_blocking/modelConfig.yml"

outputDir = "analysis/no_transmission_blocking/"


#-------------------------------------------------------------------------------
nSimulations = 0
if length(ARGS) == 1
    nSimulations = parse(Int64, ARGS[1])
else
    error("the number of simulations must be entered as a command-line argument")
end

outputFile = outputDir * "/sim_results_" * string(nSimulations) * ".csv"
outputFile = replace(outputFile, "//"=>"/")

println("")
println("READY FOR SIMULATIONS:")
println("  + Each vaccination scenario will be simulated "
        * string(nSimulations) * " times.")
println("  + Simulation results will be saved in \"" * outputFile * "\".")
println("")


#-------------------------------------------------------------------------------
DengueVacSim.print_time_stamp()

simSummary = DengueVacSim.run_full(
    modelConfigFile,
    paramFile,
    vacinnatedProbFile,
    screeningCoverageScenarios = [0.5, 0.7, 0.9],
    vaccineResponseScenarios   = [0.4, 0.6, 0.8],
    nSimulations               = nSimulations,
    nDaysPerSummary            = 364,
    vaccinateViaAging          = true,
    saveToFile                 = outputFile
)

DengueVacSim.print_time_stamp()
println("")
