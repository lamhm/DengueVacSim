require(RUtil)


## *****************************************************************************
## Load data  ----
## *****************************************************************************
setwd("/home/lamhm/Workspace/DengueVacSim/analysis/complete_transmission_blocking/vaccinate_via_aging")
# setwd("/home/lamhm/Workspace/DengueVacSim/analysis/no_transmission_blocking/vaccinate_via_aging")
# setwd("/home/lamhm/Workspace/DengueVacSim/test/")

simResultsFileName <- "sim_results_1000"

simResults <- read.csv(simResultsFileName %_% ".csv", stringsAsFactors = F)


## *****************************************************************************
## Format data  ----
## *****************************************************************************
simResults <- { function(){
    # simResults$screeningCoverage <-
    #     paste0(simResults$screeningCoverage * 100, "% Coverage")
    # simResults$vaccineResponseProb <-
    #     paste0(simResults$vaccineResponseProb * 100, "% Immuno-effectiveness")

    percentageColNames <- c(
        "avtHosp", "cumAvtHosp", "vacInducedHospPerc",
        "percNegVaccines", "percPriVaccines", "percSndVaccines"
    )
    for (colName in percentageColNames) {
        colIdx <- startsWith(names(simResults), colName)
        simResults[, colIdx] <- simResults[, colIdx] * 100
    }

    simResults$screeningScenario <- factor(
        simResults$screeningScenario,
        levels = c("no vaccine",
                   "no screening",
                   "realistic screening",
                   "perfect screening"),
        labels = c("No vaccine",
                   "Vaccine without screening",
                   "Vaccine with realistic screening",
                   "Vaccine with perfect screening"),
        ordered = T
    )

    simResults$time <- round(simResults$time)
    simResults$time <- as.factor(simResults$time)

    return(simResults)
} }()


## *****************************************************************************
## Separate the no-vaccine scenario from the vaccine ones  ----
## *****************************************************************************
noVac <- simResults[ which(
    simResults$screeningScenario == "No vaccine"
    & simResults$screeningCoverage == min(simResults$screeningCoverage)
    & simResults$vaccineResponseProb == min(simResults$vaccineResponseProb)
), ]


vac_all <- simResults[ which(simResults$screeningScenario != "No vaccine"), ]

vac_noScreen <- simResults[which(
    simResults$screeningScenario %in% c("Vaccine without screening")
), ]

vac_perfectScreen <- simResults[which(
    simResults$screeningScenario %in% c("Vaccine with perfect screening")
), ]

vac_realisticScreen <- simResults[which(
    simResults$screeningScenario %in% c("Vaccine with realistic screening")
), ]


## *****************************************************************************
## Summarise statistics  ----
## *****************************************************************************

min(noVac$totalHospCounts_025)
max(noVac$totalHospCounts_975)

min(noVac$totalHospCounts_500)
max(noVac$totalHospCounts_500)
mean(noVac$totalHospCounts_500)


View(
    vac_noScreen[which(vac_noScreen$screeningCoverage == 0.9
                       & vac_noScreen$vaccineResponseProb == 0.8), ]
)

View(
    simResults[which(
        simResults$time == 20
        & simResults$screeningScenario %in% c("Vaccine with realistic screening",
                                              "Vaccine with perfect screening")
    ), ]
)


View(
    simResults[which(
        simResults$avtHosp_500 == min(simResults$avtHosp_500)
    ), ]
)


vac <- vac_noScreen
vac <- vac_realisticScreen
vac <- vac_perfectScreen

vac_15 <- vac[which(vac$time == 15),  ]
vac_20 <- vac[which(vac$time == 20),  ]

vac_01 <- vac[which(vac$time == 1), c(4, 1:3, 24, 23, 25)]
vac_20 <- vac[which(vac$time == 20),  c(4, 1:3, 24, 23, 25)]
summary(vac_20$nVaccinesPerYear_500 / vac_01$nVaccinesPerYear_500 - 1)
summary(vac_20$nVaccinesPerYear_025 / vac_01$nVaccinesPerYear_025 - 1)
summary(vac_20$nVaccinesPerYear_975 / vac_01$nVaccinesPerYear_975 - 1)

mean(nVacIncrease)
min(nVacIncrease)
max(nVacIncrease)


cat( mean(vac_noScreen$percPriVaccines_500), " (95% CI: ",
     min(vac_noScreen$percPriVaccines_025), ", ",
     max(vac_noScreen$percPriVaccines_975), ")",
     sep = "" )

cat( mean(vac_perfectScreen$percPriVaccines_500), " (95% CI: ",
     min(vac_perfectScreen$percPriVaccines_025), ", ",
     max(vac_perfectScreen$percPriVaccines_975), ")",
     sep = "" )

cat( mean(vac_realisticScreen$percPriVaccines_500), " (95% CI: ",
     min(vac_realisticScreen$percPriVaccines_025), ", ",
     max(vac_realisticScreen$percPriVaccines_975), ")",
     sep = "" )
