using Revise
using Plots

using DengueVacSim
using CSV
using DataFrames


summaryRate = 28

modelConfigFile    = "test/data/modelConfig.yml"
vars, params = DengueVacSim.load_model_config(modelConfigFile)


DengueVacSim.set_params_for_no_screen!(params)
odeInitState = DengueVacSim.init_sys_state(vars, params)
odeSol = DengueVacSim.solve_odes(vars, params, odeInitState, true)


incidences = DengueVacSim.compute_incidences(odeSol, vars, params, summaryRate)
hospCounts = DengueVacSim.compute_hosp_counts(incidences, vars, params)
nNegVaccines, nPriVaccines, nSndVaccines =
    DengueVacSim.extract_vaccine_counts(odeSol, summaryRate)

plot(nNegVaccines, ylim = (0, maximum(nNegVaccines)))
plot(nPriVaccines)
plot(nSndVaccines)

plot( [
    DengueVacSim.compute_total_counts(hospCounts, excludedAgeGroups = 1)
] )
plot( [
    DengueVacSim.compute_total_counts(incidences)
] )


colIndexer = DengueVacSim.SIRColumnIndexer(4)

# priSusceptibles = DengueVacSim.extract_ode_class(
#     odeSol, colIndexer.priUnvacSusceptbIdx, summaryRate
# )


plot( [
    DengueVacSim.compute_total_counts(
        DengueVacSim.extract_ode_class(
            odeSol,
            [colIndexer.priImmunSusceptbIdx,
             colIndexer.priImmunRecovredIdxs...,
             colIndexer.sndImmunSusceptbIdxs...,
             colIndexer.sndImmunRecoverdIdx],
            summaryRate
        ),
        includedAgeGroups = 6:6
    )
] )


plot( [
    DengueVacSim.compute_total_counts(
        DengueVacSim.extract_ode_class(
            odeSol,
            [colIndexer.nNegShotsIdx],
            summaryRate
        ),
        includedAgeGroups = 1:11
    )
], ylim = (8.e6, 9.3e6) )


DengueVacSim.CaseModels.age_specific_hosp_rates(vars.ageGroupSizes,
                                     vars.hospRateCutoffs,
                                     params.hospRates)


odeInitState[:, colIndexer.nNegShotsIdx]
