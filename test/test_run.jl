# using Revise
using DengueVacSim


paramFile          = "test/data/partialCpr_param_estimates.csv"
vacinnatedProbFile = "test/data/vaccinated_probs.csv"
modelConfigFile    = "test/data/modelConfig.yml"

outputDir = "test/"


#-------------------------------------------------------------------------------
nSimulations = 0
if length(ARGS) == 1
    nSimulations = parse(Int64, ARGS[1])
else
    error("the number of simulations must be entered as a command-line argument")
end

outputFile = outputDir * "/sim_results_" * string(nSimulations) * ".csv"
outputFile = replace(outputFile, "//"=>"/")

println("")
println("READY FOR SIMULATIONS:")
println("  + Each vaccination scenario will be simulated "
        * string(nSimulations) * " times.")
println("  + Simulation results will be saved in \"" * outputFile * "\".")
println("")


#-------------------------------------------------------------------------------
DengueVacSim.print_time_stamp()

simSummary = DengueVacSim.run_full(
    modelConfigFile,
    paramFile,
    vacinnatedProbFile,
    screeningCoverageScenarios = [0.5, 1.0],
    vaccineResponseScenarios   = [0.5, 1.0],
    nSimulations               = nSimulations,
    nDaysPerSummary            = 364,
    vaccinateViaAging          = true,
    saveToFile                 = outputFile
)

DengueVacSim.print_time_stamp()
println("")
