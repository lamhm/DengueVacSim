using Revise
using Plots

using DengueVacSim
using CSV
using DataFrames



paramFile          = "test/data/partialCpr_param_estimates_1.csv"
vacinnatedProbFile = "test/data/vaccinated_probs_1.csv"
modelConfigFile    = "test/data/modelConfig.yml"

nSimulations  = 1
summaryRate = 364

paramValuesDf = DataFrame( CSV.File(paramFile, header = 1) )
paramValuesDf = DengueVacSim.rand_samples(paramValuesDf, nSimulations)
vaccinatedProbsDf = DataFrame( CSV.File(vacinnatedProbFile, header = 1) )
vaccinatedProbsDf = DengueVacSim.rand_samples(vaccinatedProbsDf, nSimulations)


vars, params = DengueVacSim.load_model_config(modelConfigFile)


DengueVacSim.set_params_for_no_vaccine!(params)
noVacSim = DengueVacSim.run_multiple(vars, params, paramValuesDf,
                                    nSimulations = nSimulations,
                                    nTimePointsPerSummary = summaryRate)

# DengueVacSim.set_params_for_no_screen!(params)
DengueVacSim.set_params_for_perfect_screen!(params)
vacSim = DengueVacSim.run_multiple(vars, params, paramValuesDf,
                                   nSimulations = nSimulations,
                                   nTimePointsPerSummary = summaryRate)


totalHospCounts_noVac = DengueVacSim.compute_total_hosp_counts(noVacSim)
totalHospCounts_vac = DengueVacSim.compute_total_hosp_counts(vacSim)
plot([totalHospCounts_noVac, totalHospCounts_vac])


noVacSim = noVacSim[1]
vacSim = vacSim[1]


plot( [
    DengueVacSim.compute_total_counts(vacSim.hospCounts.priUnvac)
] )
plot( [
    DengueVacSim.compute_total_counts(vacSim.hospCounts.sndUnvac)
] )
plot( [
    DengueVacSim.compute_total_counts(vacSim.hospCounts.sndVacci)
] )



plot( [
    DengueVacSim.compute_total_counts(vacSim.incidences.priUnvac, includedAgeGroups = [5])
] )
plot( [
    DengueVacSim.compute_total_counts(vacSim.incidences.sndUnvac, includedAgeGroups = [7])
] )
plot( [
    DengueVacSim.compute_total_counts(vacSim.incidences.sndVacci)
] )
plot( [
    DengueVacSim.compute_total_counts(vacSim.incidences.sndImmun,
                                      includedAgeGroups = [6],
                                      excludedAgeGroups = [0])
] )



plot( [
    DengueVacSim.compute_total_counts(noVacSim.hospCounts.sndUnvac),
    DengueVacSim.compute_total_counts(vacSim.hospCounts.sndUnvac)
] )
