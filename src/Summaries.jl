module Summaries

using DifferentialEquations

import ..ModelConfigs: ModelVars, ModelParams
import ..Odes
import ..CaseModels
import ..Util

import Base



mutable struct Summary
    nAgeGroups::Int
    nTimePoints::Int
    time::Vector{Float64}
    priUnvac::Matrix{Float64}
    priVacci::Matrix{Float64}
    priImmun::Matrix{Float64}
    sndUnvac::Matrix{Float64}
    sndVacci::Matrix{Float64}
    sndImmun::Matrix{Float64}

    function Summary(nAgeGroups::Int,
                     nTimePoints::Int,
                     time::Vector{Float64},
                     priUnvac::Matrix{Float64},
                     priVacci::Matrix{Float64},
                     priImmun::Matrix{Float64},
                     sndUnvac::Matrix{Float64},
                     sndVacci::Matrix{Float64},
                     sndImmun::Matrix{Float64})
        new( nAgeGroups, nTimePoints, time,
             priUnvac, priVacci, priImmun,
             sndUnvac, sndVacci, sndImmun )
    end

    function Summary(nAgeGroups::Int, nTimePoints::Int)
        new( nAgeGroups,
             nTimePoints,
             zeros(Float64, nTimePoints),
             zeros(Float64, nAgeGroups, nTimePoints),
             zeros(Float64, nAgeGroups, nTimePoints),
             zeros(Float64, nAgeGroups, nTimePoints),
             zeros(Float64, nAgeGroups, nTimePoints),
             zeros(Float64, nAgeGroups, nTimePoints),
             zeros(Float64, nAgeGroups, nTimePoints) )
    end
end



function Base.:-(a::Summary, b::T) :: Summary where T<:Real
    return Summary(a.nAgeGroups,
                   a.nTimePoints,
                   deepcopy(a.time),
                   a.priUnvac .- b,
                   a.priVacci .- b,
                   a.priImmun .- b,
                   a.sndUnvac .- b,
                   a.sndVacci .- b,
                   a.sndImmun .- b)
end



function Base.:-(a::T, b::Summary) :: Summary where T<:Real
    return Summary(b.nAgeGroups,
                   b.nTimePoints,
                   deepcopy(b.time),
                   a .- b.priUnvac,
                   a .- b.priVacci,
                   a .- b.priImmun,
                   a .- b.sndUnvac,
                   a .- b.sndVacci,
                   a .- b.sndImmun)
end



function Base.:-(a::Summary, b::Summary) :: Summary
    if a.nAgeGroups == b.nAgeGroups && a.nTimePoints == b.nTimePoints
        return Summary(a.nAgeGroups,
                       a.nTimePoints,
                       deepcopy(a.time),
                       a.priUnvac - b.priUnvac,
                       a.priVacci - b.priVacci,
                       a.priImmun - b.priImmun,
                       a.sndUnvac - b.sndUnvac,
                       a.sndVacci - b.sndVacci,
                       a.sndImmun - b.sndImmun)
    end
    throw(ErrorException(
        "cannot perform subtraction between Summaries of different sizes."
    ))
end



function Base.:/(a::Summary, b::T) :: Summary where T<:Real
    return Summary(a.nAgeGroups,
                   a.nTimePoints,
                   deepcopy(a.time),
                   a.priUnvac ./ b,
                   a.priVacci ./ b,
                   a.priImmun ./ b,
                   a.sndUnvac ./ b,
                   a.sndVacci ./ b,
                   a.sndImmun ./ b)
end



function Base.:/(a::Summary, b::Summary) :: Summary
    if a.nAgeGroups == b.nAgeGroups && a.nTimePoints == b.nTimePoints
        return Summary(a.nAgeGroups,
                       a.nTimePoints,
                       deepcopy(a.time),
                       a.priUnvac ./ b.priUnvac,
                       a.priVacci ./ b.priVacci,
                       a.priImmun ./ b.priImmun,
                       a.sndUnvac ./ b.sndUnvac,
                       a.sndVacci ./ b.sndVacci,
                       a.sndImmun ./ b.sndImmun)
    end
    throw(ErrorException(
        "cannot perform division between Summaries of different sizes."
    ))
end



function compute_incidences(odeSolution::ODESolution,
                            vars::ModelVars,
                            params::ModelParams,
                            nTimePointsPerSummary::Int = 1) :: Summary
    colIndexer = Odes.SIRColumnIndexer(vars.nSerotypes)
    nAgeGroups  = length(vars.ageGroupSizes)
    nOdeTimePoints = length(odeSolution.t)

    nSummaryTimePoints = ceil(Int, nOdeTimePoints / nTimePointsPerSummary)
    incidences = Summary(nAgeGroups, nSummaryTimePoints)

    summaryTimePointIdx = 1
    for odeTimePointIdx = 1 : nOdeTimePoints
        odeTime = odeSolution.t[odeTimePointIdx]
        sysState = odeSolution.u[odeTimePointIdx]
        Util.zerofy_negative_numbers!(sysState)

        priFois = Odes.pri_fois(sysState, vars, params, odeTime)
        totalPriFoi = sum(priFois)
        totalSndFoi = totalPriFoi .- priFois

        S0u = sysState[:, colIndexer.priUnvacSusceptbIdx]
        S0v = sysState[:, colIndexer.priVacciSusceptbIdx]
        S0i = sysState[:, colIndexer.priImmunSusceptbIdx]

        incidences.priUnvac[:, summaryTimePointIdx] += S0u * totalPriFoi
        incidences.priVacci[:, summaryTimePointIdx] += S0v * totalPriFoi
        incidences.priImmun[:, summaryTimePointIdx] += S0i * totalPriFoi

        S1u = sysState[:, colIndexer.sndUnvacSusceptbIdxs]
        S1v = sysState[:, colIndexer.sndVacciSusceptbIdxs]
        S1i = sysState[:, colIndexer.sndImmunSusceptbIdxs]

        incidences.sndUnvac[:, summaryTimePointIdx] +=
            sum(S1u .* transpose(totalSndFoi), dims = 2)
        incidences.sndVacci[:, summaryTimePointIdx] +=
            sum(S1v .* transpose(totalSndFoi), dims = 2)
        incidences.sndImmun[:, summaryTimePointIdx] +=
            sum(S1i .* transpose(totalSndFoi), dims = 2)

        if odeTimePointIdx % nTimePointsPerSummary == 0
            incidences.time[summaryTimePointIdx] =
                Odes.convert_ode_time_to_sim_year(
                    odeTime + vars.nDaysReportingLag, vars)
            summaryTimePointIdx += 1
        end
    end

    return (incidences)
end # function




"""
Calculate the number of hospitalisations based on the given primary and
secondary incidences.
"""
function compute_hosp_counts(incidences::Summary,
                             vars::ModelVars,
                             params::ModelParams) :: Summary

    maternalEffects = CaseModels.maternal_effects(vars, params)
    ageSpecHospRates = CaseModels.age_specific_hosp_rates(vars, params)
    priSymptomaticProb = params.priSymptomaticProb
    sndSymptomaticProb = params.sndSymptomaticProb

    hospCounts = Summary(incidences.nAgeGroups, incidences.nTimePoints)
    hospCounts.time = incidences.time

    # Primary cases that are not affected by vaccination
    hospCounts.priUnvac = incidences.priUnvac .* maternalEffects .*
                          priSymptomaticProb .* ageSpecHospRates
    hospCounts.priVacci = incidences.priVacci .* maternalEffects .*
                          priSymptomaticProb .* ageSpecHospRates

    # Primary cases that experience disease enhancement due to vaccination
    hospCounts.priImmun = incidences.priImmun .*
                          sndSymptomaticProb .* ageSpecHospRates

    # Secondary cases that are not affected by vaccination
    hospCounts.sndUnvac = incidences.sndUnvac .*
                          sndSymptomaticProb .* ageSpecHospRates
    hospCounts.sndVacci = incidences.sndVacci .*
                          sndSymptomaticProb .* ageSpecHospRates

    # hospCounts.sndImmun is 0 since effectively vaccinated individuals are
    # assumed to be completely asymptomatic during secondary infections.

    return hospCounts
end



function compute_total_counts(
            summary::Summary;
            includedAgeGroups::Union{Int, UnitRange{Int}, Vector{Int}, Nothing}
                = nothing,
            excludedAgeGroups::Union{Int, UnitRange{Int}, Vector{Int}, Nothing}
                = nothing
            ) :: Vector{Float64}

    totalMatrix = summary.priUnvac + summary.priVacci + summary.priImmun +
                  summary.sndUnvac + summary.sndVacci + summary.sndImmun
    return compute_total_counts(totalMatrix,
                                includedAgeGroups = includedAgeGroups,
                                excludedAgeGroups = excludedAgeGroups)
end



function compute_total_counts(
            countData::Matrix{Float64};
            includedAgeGroups::Union{Int, UnitRange{Int}, Vector{Int}, Nothing}
                = nothing,
            excludedAgeGroups::Union{Int, UnitRange{Int}, Vector{Int}, Nothing}
                = nothing
            ) :: Vector{Float64}

    selectedGroups = [i for i = 1 : size(countData, 1)]
    if includedAgeGroups != nothing
        selectedGroups = Vector(includedAgeGroups)
    end
    if excludedAgeGroups != nothing
        selectedGroups = setdiff(selectedGroups, excludedAgeGroups)
    end

    return sum(countData[selectedGroups, :], dims = 1)[1, :]
end



function sum_across_ages(data::Matrix{Float64}) :: Vector{Float64}
    return sum(data, dims = 1)[1, :]
end



function extract_vaccine_counts(
                odeSolution::ODESolution,
                thiningRate::Int = 1
                ) :: Tuple{Vector{Float64}, Vector{Float64}, Vector{Float64}}
    nOdeClasses = size(odeSolution.u[1], 2)

    nNegShots = Odes.extract_ode_class(odeSolution, nOdeClasses - 2, 1)
    nPriShots = Odes.extract_ode_class(odeSolution, nOdeClasses - 1, 1)
    nSndShots = Odes.extract_ode_class(odeSolution, nOdeClasses, 1)

    selectedTimePoints = filter( x -> x % thiningRate == 0,
                                 1 : size(nNegShots, 2) )

    return ( sum(nNegShots, dims = 1)[1, selectedTimePoints],
             sum(nPriShots, dims = 1)[1, selectedTimePoints],
             sum(nSndShots, dims = 1)[1, selectedTimePoints] )
end





end  # module Summaries
