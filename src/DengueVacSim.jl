module DengueVacSim

include("Util.jl")
include("ModelConfigs.jl")
include("Odes.jl")
include("CaseModels.jl")
include("Summaries.jl")

using .Util: rand_samples, print_time_stamp, row_quantile
using .ModelConfigs: load_model_config, update_params!,
                     set_params_for_no_vaccine!, set_params_for_no_screen!,
                     set_params_for_perfect_screen!,
                     set_vaccinated_probs!
using .Odes: SIRColumnIndexer, init_sys_state, solve_odes, extract_ode_class,
             redistribute_serotypes!
using .CaseModels
using .Summaries: Summary, compute_incidences, compute_hosp_counts,
                  compute_total_counts, sum_across_ages,
                  extract_vaccine_counts

using DataFrames: DataFrame, coalesce
using CSV
using Formatting: sprintf1
using Dates
using ProgressMeter


struct SimResult
    incidences::Summary
    hospCounts::Summary
    nNegVaccines::Vector{Float64}
    nPriVaccines::Vector{Float64}
    nSndVaccines::Vector{Float64}
end



function run_single(vars::ModelConfigs.ModelVars,
                    params::ModelConfigs.ModelParams,
                    odeInitState::Matrix{Float64},
                    nTimePointsPerSummary::Int = 1;
                    vaccinateViaAging::Bool    = true) :: SimResult

    odeSol = solve_odes(vars, params, odeInitState, vaccinateViaAging)

    incidences = compute_incidences(odeSol, vars, params, nTimePointsPerSummary)
    hospCounts = compute_hosp_counts(incidences, vars, params)
    nNegVaccines, nPriVaccines, nSndVaccines =
        extract_vaccine_counts(odeSol, nTimePointsPerSummary)

    return SimResult(incidences, hospCounts,
                     nNegVaccines, nPriVaccines, nSndVaccines)
end



function run_multiple(vars::ModelConfigs.ModelVars,
                      params::ModelConfigs.ModelParams,
                      paramValuesDf::DataFrame,
                      vaccinatedProbsDf::Union{Nothing, DataFrame} = nothing;
                      nSimulations::Int = 0,
                      nTimePointsPerSummary::Int = 1,
                      progress::Union{Nothing, Progress} = nothing,
                      vaccinateViaAging::Bool            = true
                      ) :: Vector{SimResult}

    odeInitState = init_sys_state(vars, params,
                                  vaccinateViaAging = vaccinateViaAging)
    nParamRows = size(paramValuesDf, 1)

    if nSimulations <= 0
        nSimulations = nParamRows
    end

    if nSimulations != nParamRows
        paramValuesDf = rand_samples(paramValuesDf, nSimulations)
    end
    if vaccinatedProbsDf != nothing && size(vaccinatedProbsDf, 1) != nSimulations
        vaccinatedProbsDf = rand_samples(vaccinatedProbsDf, nSimulations)
    end

    result = Vector{SimResult}(undef, nSimulations)
    serotypeDist = Vector{Float64}(undef, vars.nSerotypes)
    for simIdx = 1 : nSimulations
        for i in 1 : vars.nSerotypes
            serotypeDist[i] =
                paramValuesDf[simIdx, Symbol("serotypeProportions_" * string(i))]
        end
        redistribute_serotypes!(odeInitState, vars.nSerotypes, serotypeDist)

        update_params!(params, vars, paramValuesDf, simIdx)
        if vaccinatedProbsDf != nothing
            set_vaccinated_probs!(params, vaccinatedProbsDf[simIdx, 1:3]...)
        end
        result[simIdx] = run_single(
            vars, params, odeInitState, nTimePointsPerSummary,
            vaccinateViaAging = vaccinateViaAging
        )
        if progress != nothing
            next!(progress)
        end
    end

    return result
end



function compute_averted_hosp(noVaccSim::Vector{SimResult},
                              vaccineSim::Vector{SimResult};
                              saveToFile::String = ""
                              ) :: Matrix{Float64}
    nSimulations = length(vaccineSim)
    avtHosp = Vector{Vector{Float64}}(undef, nSimulations)
    for simIdx = 1 : nSimulations
        noVaccSimTotalHosp = compute_total_counts(noVaccSim[simIdx].hospCounts,
                                                  excludedAgeGroups = 1)
        vaccinSimTotalHosp = compute_total_counts(vaccineSim[simIdx].hospCounts,
                                                  excludedAgeGroups = 1)
        avtHosp[simIdx] = 1.0 .- (vaccinSimTotalHosp ./ noVaccSimTotalHosp)
    end

    if length(saveToFile) > 0
        avtHospDf = DataFrame(
            avtHosp,
            [Symbol("sim_" * string(i)) for i = 1 : nSimulations]
        )
        CSV.write(saveToFile, avtHospDf)
    end

    return hcat(avtHosp...)
end



function compute_cum_averted_hosp(noVaccSim::Vector{SimResult},
                                  vaccineSim::Vector{SimResult};
                                  saveToFile::String = ""
                                  ) :: Matrix{Float64}
    nSimulations = length(vaccineSim)
    cumAvtHosp = Vector{Vector{Float64}}(undef, nSimulations)
    for simIdx = 1 : nSimulations
        noVaccSimTotalHosp = compute_total_counts(noVaccSim[simIdx].hospCounts,
                                                  excludedAgeGroups = 1)
        cumsum!(noVaccSimTotalHosp, noVaccSimTotalHosp)

        vaccinSimTotalHosp = compute_total_counts(vaccineSim[simIdx].hospCounts,
                                                  excludedAgeGroups = 1)
        cumsum!(vaccinSimTotalHosp, vaccinSimTotalHosp)

        cumAvtHosp[simIdx] = 1.0 .- (vaccinSimTotalHosp ./ noVaccSimTotalHosp)
    end

    if length(saveToFile) > 0
        cumAvtHospDf = DataFrame(
            cumAvtHosp,
            [Symbol("sim_" * string(i)) for i = 1 : nSimulations]
        )
        CSV.write(saveToFile, avtHospDf)
    end

    return hcat(cumAvtHosp...)
end



function compute_total_hosp_counts(vaccineSim::Vector{SimResult};
                                   saveToFile::String = ""
                                   ) :: Matrix{Float64}
    nSimulations = length(vaccineSim)
    totalHospCounts = Vector{Vector{Float64}}(undef, nSimulations)
    for simIdx = 1 : nSimulations
        totalHospCounts[simIdx] = compute_total_counts(vaccineSim[simIdx].hospCounts,
                                                       excludedAgeGroups = 1)
    end

    if length(saveToFile) > 0
        totalHospCountsDf = DataFrame(
            totalHospCounts,
            [Symbol("sim_" * string(i)) for i = 1 : nSimulations]
        )
        CSV.write(saveToFile, totalHospCountsDf)
    end

    return hcat(totalHospCounts...)
end



function compute_vaccine_induced_hosp_count(vaccineSim::Vector{SimResult};
                                            saveToFile::String = ""
                                            ) :: Matrix{Float64}
    nSimulations = length(vaccineSim)
    vaccineInducedHospCount = Vector{Vector{Float64}}(undef, nSimulations)
    for simIdx = 1 : nSimulations
        vaccineInducedHospCount[simIdx] = compute_total_counts(
            vaccineSim[simIdx].hospCounts.priImmun,
            excludedAgeGroups = 1
        )
    end

    if length(saveToFile) > 0
        vaccineInducedHospCountDf = DataFrame(
            vaccineInducedHospCount,
            [Symbol("sim_" * string(i)) for i = 1 : nSimulations]
        )
        CSV.write(saveToFile, vaccineInducedHospCountDf)
    end

    return hcat(vaccineInducedHospCount...)
end


function compute_vaccine_induced_hosp_perc( vaccineSim::Vector{SimResult};
                                            saveToFile::String = ""
                                            ) :: Matrix{Float64}
    nSimulations = length(vaccineSim)
    vaccineInducedHospPerc = Vector{Vector{Float64}}(undef, nSimulations)
    for simIdx = 1 : nSimulations
        vaccinSimTotalHosp = compute_total_counts(vaccineSim[simIdx].hospCounts,
                                                  excludedAgeGroups = 1)
        priImmuHosp = compute_total_counts(vaccineSim[simIdx].hospCounts.priImmun,
                                           excludedAgeGroups = 1)
        vaccineInducedHospPerc[simIdx] = priImmuHosp ./ vaccinSimTotalHosp
    end

    if length(saveToFile) > 0
        vaccineInducedHospPercDf = DataFrame(
            vaccineInducedHospPerc,
            [Symbol("sim_" * string(i)) for i = 1 : nSimulations]
        )
        CSV.write(saveToFile, vaccineInducedHospPercDf)
    end

    return hcat(vaccineInducedHospPerc...)
end


function compute_total_vaccine_shots(vaccineSim::Vector{SimResult};
                                     saveToFile::String = ""
                                     ) :: Matrix{Float64}
    nSimulations = length(vaccineSim)
    totalShots = Vector{Vector{Float64}}(undef, nSimulations)
    for simIdx = 1 : nSimulations
        totalShots[simIdx] = vaccineSim[simIdx].nNegVaccines +
                             vaccineSim[simIdx].nPriVaccines +
                             vaccineSim[simIdx].nSndVaccines
    end

    if length(saveToFile) > 0
        totalShotsDf = DataFrame(
            totalShots,
            [Symbol("sim_" * string(i)) for i = 1 : nSimulations]
        )
        CSV.write(saveToFile, totalShotsDf)
    end

    return hcat(totalShots...)
end



function summary_sims(noVaccMultiSims::Vector{SimResult},
                      vaccinMultiSims::Vector{SimResult};
                      screeningCoverage::Float64 = NaN,
                      vaccineResponseProb::Float64 = NaN,
                      scenarioName::String = "") :: DataFrame
    quantileProbs = [0.025, 0.5, 0.975]
    time = noVaccMultiSims[1].hospCounts.time
    nTimePoints = length(time)

    summary = DataFrame(
        time = time,
        screeningCoverage = screeningCoverage,
        vaccineResponseProb = vaccineResponseProb,
        screeningScenario = scenarioName
    )

    totalHospCounts = compute_total_hosp_counts(vaccinMultiSims)
    summary = hcat(
        summary,
        row_quantile(totalHospCounts, quantileProbs, "totalHospCounts_"),
        copycols = false
    )

    if noVaccMultiSims !== vaccinMultiSims
        vacInducedHospCount = compute_vaccine_induced_hosp_count(vaccinMultiSims)
        vacInducedHospPerc = compute_vaccine_induced_hosp_perc(vaccinMultiSims)
        avtHosp = compute_averted_hosp(noVaccMultiSims, vaccinMultiSims)
        cumAvtHosp = compute_cum_averted_hosp(noVaccMultiSims, vaccinMultiSims)

        nVaccines = compute_total_vaccine_shots(vaccinMultiSims)
        nVaccinesPerYear =
            nVaccines - vcat( transpose(zeros(Float64, size(nVaccines, 2))),
                              nVaccines[1:end-1, :] )

        nSimulations = length(vaccinMultiSims)
        nNegVaccinesPerYear = Matrix{Float64}(undef, nTimePoints, nSimulations)
        nPriVaccinesPerYear = Matrix{Float64}(undef, nTimePoints, nSimulations)
        nSndVaccinesPerYear = Matrix{Float64}(undef, nTimePoints, nSimulations)
        for simIdx = 1 : nSimulations
            nNegVaccinesPerYear[:, simIdx] =
                vaccinMultiSims[simIdx].nNegVaccines -
                vcat(0.0, vaccinMultiSims[simIdx].nNegVaccines[1:end-1])

            nPriVaccinesPerYear[:, simIdx] =
                vaccinMultiSims[simIdx].nPriVaccines -
                vcat(0.0, vaccinMultiSims[simIdx].nPriVaccines[1:end-1])

            nSndVaccinesPerYear[:, simIdx] =
                vaccinMultiSims[simIdx].nSndVaccines -
                vcat(0.0, vaccinMultiSims[simIdx].nSndVaccines[1:end-1])
        end

        summary = hcat(
            summary,
            row_quantile(vacInducedHospCount, quantileProbs, "vacInducedHospCount_"),
            row_quantile(vacInducedHospPerc, quantileProbs, "vacInducedHospPerc_"),
            row_quantile(avtHosp, quantileProbs, "avtHosp_"),
            row_quantile(cumAvtHosp, quantileProbs, "cumAvtHosp_"),
            row_quantile(nVaccines, quantileProbs, "nVaccines_"),
            row_quantile(nVaccinesPerYear, quantileProbs, "nVaccinesPerYear_"),
            row_quantile(nNegVaccinesPerYear ./ nVaccinesPerYear, quantileProbs,
                         "percNegVaccines_"),
            row_quantile(nPriVaccinesPerYear ./ nVaccinesPerYear, quantileProbs,
                         "percPriVaccines_"),
            row_quantile(nSndVaccinesPerYear ./ nVaccinesPerYear, quantileProbs,
                         "percSndVaccines_"),
            copycols = false
        )
    end

    return summary
end



function run_full(modelConfigFile::String,
                  paramFile::String,
                  vacinnatedProbFile::String;
                  screeningCoverageScenarios::Vector{Float64},
                  vaccineResponseScenarios::Vector{Float64},
                  nSimulations::Int,
                  nDaysPerSummary::Int,
                  vaccinateViaAging::Bool = true,
                  saveToFile::String      = "") :: DataFrame

    ## Load model input
    paramValuesDf = DataFrame( CSV.File(paramFile, header = 1) )
    paramValuesDf = rand_samples(paramValuesDf, nSimulations)

    vaccinatedProbsDf = DataFrame( CSV.File(vacinnatedProbFile, header = 1) )
    vaccinatedProbsDf = rand_samples(vaccinatedProbsDf, nSimulations)

    vars, params = load_model_config(modelConfigFile)

    ## Simulate the no-vaccine scenario
    progress = Progress(nSimulations, dt = 1.0,
                        desc = "Simulating the baseline scenario :  ")
    set_params_for_no_vaccine!(params)
    noVaccSim = run_multiple(
        vars, params, paramValuesDf,
        nSimulations          = nSimulations,
        nTimePointsPerSummary = nDaysPerSummary,
        progress              = progress,
        vaccinateViaAging     = vaccinateViaAging
    )

    ## Simulate vaccine scenarios
    nProgressSteps = length(screeningCoverageScenarios) *
                     length(vaccineResponseScenarios) * (3 * nSimulations + 1)
    progress = Progress(nProgressSteps, dt = 1.0,
                        desc = "Simulating vaccine scenarios     :  ")

    scenarioNames = ["no screening", "perfect screening", "realistic screening"]
    summary = DataFrame()

    for screeningCoverage in screeningCoverageScenarios
        params.screeningCoverages =
            (params.screeningCoverages .> 0) .* screeningCoverage

        for vaccineResponseProb in vaccineResponseScenarios
            params.vaccineResponseProb = vaccineResponseProb
            simResults = Vector{Vector{SimResult}}(undef, 3)

            set_params_for_no_screen!(params)
            simResults[1] = run_multiple(
                vars, params, paramValuesDf,
                nSimulations          = nSimulations,
                nTimePointsPerSummary = nDaysPerSummary,
                progress              = progress,
                vaccinateViaAging     = vaccinateViaAging
            )

            set_params_for_perfect_screen!(params)
            simResults[2] = run_multiple(
                vars, params, paramValuesDf,
                nSimulations          = nSimulations,
                nTimePointsPerSummary = nDaysPerSummary,
                progress              = progress,
                vaccinateViaAging     = vaccinateViaAging
            )

            simResults[3] = run_multiple(
                vars, params, paramValuesDf, vaccinatedProbsDf,
                nSimulations          = nSimulations,
                nTimePointsPerSummary = nDaysPerSummary,
                progress              = progress,
                vaccinateViaAging     = vaccinateViaAging
            )

            for scenarioIdx = 1 : 3
                vaccineSummary = summary_sims(
                    noVaccSim,
                    simResults[scenarioIdx],
                    screeningCoverage = screeningCoverage,
                    vaccineResponseProb = vaccineResponseProb,
                    scenarioName = scenarioNames[scenarioIdx]
                )
                summary = vcat(summary, vaccineSummary, cols = :union)
            end

            noVacSummary = summary_sims(
                noVaccSim,
                noVaccSim,
                screeningCoverage = screeningCoverage,
                vaccineResponseProb = vaccineResponseProb,
                scenarioName = "no vaccine"
            )
            summary = vcat(summary, noVacSummary, cols = :union)
            summary = coalesce.(summary, 0.0) # replace missing values by 0s
            next!(progress)
        end
    end

    if length(saveToFile) > 0
        CSV.write(saveToFile, format_dataframe(summary))
    end

    return summary
end



function format_dataframe(source::DataFrame) :: DataFrame
    result = deepcopy(source)

    for col = 1 : size(result, 2)
        if eltype(result[!, col]) == Float64
            result[!, col] = sprintf1.("%.8f", result[!, col])
        end
    end

    return result
end


end # module
