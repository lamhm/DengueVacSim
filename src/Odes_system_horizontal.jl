"""
The ODE system of the SIR model. This function should only by used by the ODE
solver.
"""
function ode_system_vaccinate_within_age_groups(
                diffs::Matrix{Float64},
                sysState::Matrix{Float64},
                modelConfig::Tuple{ModelVars, ModelParams},
                odeTime::Float64)

    vars, params = modelConfig

    nSerotypes = vars.nSerotypes
    nAgeGroups = length(vars.ageGroupSizes)

    # Calculate seasonal birth rate
    odeYearIdx, odeDayOfYear = intepret_ode_time(odeTime, vars)
    birthSeasonLength = vars.nDaysPerYear / length(vars.seasonalBirthRates)
    birthSeasonIdx    = Int(ceil(odeDayOfYear / birthSeasonLength))
    seasonalBirthRate = length(vars.seasonalBirthRates) *
                        vars.seasonalBirthRates[birthSeasonIdx] /
                        sum(vars.seasonalBirthRates)

    # Calculate population growth
    instantNatPopGrowthRate = 0.0
    instantMigrationRate    = 0.0
    if odeYearIdx >= 0 && odeYearIdx < vars.nSimYears
        instantNatPopGrowthRate = convert_annual_rate_to_daily_rate(
            vars.annualNatPopGrowthRates[odeYearIdx + 1] - 1.0,
            vars.nDaysPerYear
        )
        instantMigrationRate = convert_annual_rate_to_daily_rate(
            vars.annualNetMigRates[odeYearIdx + 1],
            vars.nDaysPerYear
        )

    elseif length(vars.annualNatPopGrowthRates) > 0
        instantNatPopGrowthRate = convert_annual_rate_to_daily_rate(
            minimum(vars.annualNatPopGrowthRates) - 1.0,
            vars.nDaysPerYear
        )
    end

    # Retrieve vaccine coverage
    screeningCoverages = zeros(Float64, nAgeGroups)
    if odeYearIdx >= vars.simYearToStartVaccination &&
            vars.simYearToStartVaccination >= 0
        screeningCoverages = convert_annual_rate_to_daily_rate(
            params.screeningCoverages,
            vars.nDaysPerYear
        )
    end
    # unscreenedProbs = 1.0 .- screeningCoverages
    unscreenedProbs = 1.0

    # Other vaccine-related parameters
    vaccineResponseProb = params.vaccineResponseProb
    negVaccinatedProb   = params.negVaccinatedProb
    priVaccinatedProb   = params.priVaccinatedProb
    sndVaccinatedProb   = params.sndVaccinatedProb

    Util.zerofy_negative_numbers!(sysState)
    colIndexer = SIRColumnIndexer(nSerotypes)

    S0u = sysState[:, colIndexer.priUnvacSusceptbIdx]
    S0v = sysState[:, colIndexer.priVacciSusceptbIdx]
    S0i = sysState[:, colIndexer.priImmunSusceptbIdx]

    I1u = sysState[:, colIndexer.priUnvacInfectedIdxs]
    I1v = sysState[:, colIndexer.priVacciInfectedIdxs]
    I1i = sysState[:, colIndexer.priImmunInfectedIdxs]

    R1u = sysState[:, colIndexer.priUnvacRecovredIdxs]
    R1v = sysState[:, colIndexer.priVacciRecovredIdxs]
    R1i = sysState[:, colIndexer.priImmunRecovredIdxs]

    S1u = sysState[:, colIndexer.sndUnvacSusceptbIdxs]
    S1v = sysState[:, colIndexer.sndVacciSusceptbIdxs]
    S1i = sysState[:, colIndexer.sndImmunSusceptbIdxs]

    I2u = sysState[:, colIndexer.sndUnvacInfectedIdxs]
    I2v = sysState[:, colIndexer.sndVacciInfectedIdxs]
    I2i = sysState[:, colIndexer.sndImmunInfectedIdxs]

    IPu = sysState[:, colIndexer.cprUnvacInfectedIdxs]
    IPv = sysState[:, colIndexer.cprVacciInfectedIdxs]

    R2u = sysState[:, colIndexer.sndUnvacRecoverdIdx]
    R2v = sysState[:, colIndexer.sndVacciRecoverdIdx]
    R2i = sysState[:, colIndexer.sndImmunRecoverdIdx]

    # Calculate primary and secondary FOIs
    priFois = pri_fois(sysState, vars, params, odeTime)
    totalPriFoi = sum(priFois)
    totalSndFoi = totalPriFoi .- priFois
    sndFois = repeat(transpose(priFois), nSerotypes, 1)
    sndFois[1 : (nSerotypes + 1) : end] .= 0  # make the diagonal become zeros


    # PRIMARY SUSCEPTIBLE
    # Derivatives of S0u (without aging & migration)
    nRemovedByInfection = S0u .* unscreenedProbs * totalPriFoi
    nRemovedByVaccination = S0u .* screeningCoverages * negVaccinatedProb
    diffs[:, colIndexer.priUnvacSusceptbIdx] =
        -nRemovedByInfection - nRemovedByVaccination

    # Derivatives of S0v (without aging & migration)
    nRemovedByInfection = S0v * totalPriFoi
    nAddedByVaccination = S0u .* screeningCoverages *
                          (negVaccinatedProb * (1.0 - vaccineResponseProb))
    diffs[:, colIndexer.priVacciSusceptbIdx] =
        nAddedByVaccination - nRemovedByInfection

    # Derivatives of S0i (without aging & migration)
    nRemovedByInfection = S0i * totalPriFoi
    nAddedByVaccination = S0u .* screeningCoverages *
                          (negVaccinatedProb * vaccineResponseProb)
    diffs[:, colIndexer.priImmunSusceptbIdx] =
        nAddedByVaccination - nRemovedByInfection


    # PRIMARY INFECTED
    # Derivatives of I1u (without aging & migration)
    nAddedByInfection  = S0u * transpose(priFois)
    nRemovedByRecovery = I1u * params.recoveryRate
    diffs[:, colIndexer.priUnvacInfectedIdxs] =
        nAddedByInfection - nRemovedByRecovery

    # Derivatives of I1v (without aging & migration)
    nAddedByInfection  = S0v * transpose(priFois)
    nRemovedByRecovery = I1v * params.recoveryRate
    diffs[:, colIndexer.priVacciInfectedIdxs] =
        nAddedByInfection - nRemovedByRecovery

    # Derivatives of I1i (without aging & migration)
    nAddedByInfection  = S0i * transpose(priFois)
    nRemovedByRecovery = I1i * params.recoveryRate
    diffs[:, colIndexer.priImmunInfectedIdxs] =
        nAddedByInfection - nRemovedByRecovery


    # PRIMARY RECOVERED
    # Derivatives of R1u (without aging & migration)
    nAddedByRecovery = I1u * params.recoveryRate
    nRemovedByImmWaning = R1u / params.crossProtectDuration
    nRemovedByInfection =
        R1u .* unscreenedProbs .* transpose(totalSndFoi) *
        params.infectRiskDuringPriCpr
    nRemovedByVaccination = R1u .* screeningCoverages * priVaccinatedProb
    diffs[:, colIndexer.priUnvacRecovredIdxs] =
        ( + nAddedByRecovery - nRemovedByImmWaning
          - nRemovedByInfection - nRemovedByVaccination )

    # Derivatives of R1v (without aging & migration)
    nAddedByRecovery = I1v * params.recoveryRate
    nRemovedByImmWaning = R1v / params.crossProtectDuration
    nRemovedByInfection =
        R1v .* transpose(totalSndFoi) * params.infectRiskDuringPriCpr
    nAddedByVaccination = R1u .* screeningCoverages *
                          (priVaccinatedProb * (1.0 - vaccineResponseProb))
    diffs[:, colIndexer.priVacciRecovredIdxs] =
        ( + nAddedByRecovery + nAddedByVaccination
          - nRemovedByImmWaning - nRemovedByInfection )

    # Derivatives of R1i (without aging & migration)
    nAddedByRecovery = I1i * params.recoveryRate
    nRemovedByImmWaning = R1i / params.crossProtectDuration
    nRemovedByInfection =
        R1i .* transpose(totalSndFoi) * params.infectRiskDuringPriCpr
    nAddedByVaccination =
        R1u .* screeningCoverages * (priVaccinatedProb * vaccineResponseProb)
    diffs[:, colIndexer.priImmunRecovredIdxs] =
        ( + nAddedByRecovery + nAddedByVaccination
          - nRemovedByImmWaning - nRemovedByInfection )


    # SECONDARY SUSCEPTIBLE
    # Derivatives of S1u (without aging & migration)
    nAddedByImmWaning  = R1u / params.crossProtectDuration
    nRemovedByInfection = S1u .* unscreenedProbs .* transpose(totalSndFoi)
    nRemovedByVaccination = S1u .* screeningCoverages * priVaccinatedProb
    diffs[:, colIndexer.sndUnvacSusceptbIdxs] =
        nAddedByImmWaning - nRemovedByInfection - nRemovedByVaccination

    # Derivatives of S1v (without aging & migration)
    nAddedByImmWaning  = R1v / params.crossProtectDuration
    nRemovedByInfection = S1v .* transpose(totalSndFoi)
    nAddedByVaccination = S1u .* screeningCoverages *
                          (priVaccinatedProb * (1.0 - vaccineResponseProb))
    diffs[:, colIndexer.sndVacciSusceptbIdxs] =
        nAddedByImmWaning + nAddedByVaccination - nRemovedByInfection

    # Derivatives of S1i (without aging & migration)
    nAddedByImmWaning  = R1i / params.crossProtectDuration
    nRemovedByInfection = S1i .* transpose(totalSndFoi)
    nAddedByVaccination = S1u .* screeningCoverages *
                          (priVaccinatedProb * vaccineResponseProb)
    diffs[:, colIndexer.sndImmunSusceptbIdxs] =
        nAddedByImmWaning + nAddedByVaccination - nRemovedByInfection


    # SECONDARY INFECTED
    # Derivatives of I2u (without aging & migration)
    nAddedByInfection  = S1u * sndFois
    nRemovedByRecovery = I2u * params.recoveryRate
    diffs[:, colIndexer.sndUnvacInfectedIdxs] =
        nAddedByInfection - nRemovedByRecovery

    # Derivatives of I2v (without aging & migration)
    nAddedByInfection  = S1v * sndFois
    nRemovedByRecovery = I2v * params.recoveryRate
    diffs[:, colIndexer.sndVacciInfectedIdxs] =
        nAddedByInfection - nRemovedByRecovery

    # Derivatives of I2i (without aging & migration)
    nInfectDuringPriCpr = params.infectRiskDuringPriCpr * R1i * sndFois
    nInfectDuringSusctb = S1i * sndFois
    nRemovedByRecovery = I2i * params.recoveryRate
    diffs[:, colIndexer.sndImmunInfectedIdxs] =
        nInfectDuringPriCpr + nInfectDuringSusctb - nRemovedByRecovery


    # INFECTED DURING PRIMARY CROSS-PROTECTION
    # Derivatives of IPu (without aging & migration)
    nAddedByInfection = params.infectRiskDuringPriCpr * R1u * sndFois
    nRemovedByRecovery = IPu * params.recoveryRate
    diffs[:, colIndexer.cprUnvacInfectedIdxs] =
        nAddedByInfection - nRemovedByRecovery

    # Derivatives of IPv (without aging & migration)
    nAddedByInfection = params.infectRiskDuringPriCpr * R1v * sndFois
    nRemovedByRecovery = IPv * params.recoveryRate
    diffs[:, colIndexer.cprVacciInfectedIdxs] =
        nAddedByInfection - nRemovedByRecovery


    # SECONDARY RECOVERED
    # Derivatives of R2u (without aging & migration)
    nAddedByRecovery = sum((IPu + I2u), dims = 2) * params.recoveryRate
    nRemovedByVaccination = R2u .* screeningCoverages * sndVaccinatedProb
    diffs[:, colIndexer.sndUnvacRecoverdIdx] =
        nAddedByRecovery - nRemovedByVaccination

    # Derivatives of R2v (without aging & migration)
    nAddedByRecovery = sum((IPv + I2v), dims = 2) * params.recoveryRate
    nAddedByVaccination = R2u .* screeningCoverages *
                          (sndVaccinatedProb * (1.0 - vaccineResponseProb))
    diffs[:, colIndexer.sndVacciRecoverdIdx] =
        nAddedByRecovery + nAddedByVaccination

    # Derivatives of R2i (without aging & migration)
    nAddedByRecovery = sum(I2i, dims = 2) * params.recoveryRate
    nAddedByVaccination = R2u .* screeningCoverages *
                          (sndVaccinatedProb * vaccineResponseProb)
    diffs[:, colIndexer.sndImmunRecoverdIdx] =
        nAddedByRecovery + nAddedByVaccination


    # Additional quantities for immigration
    if vars.noVacBeforeImmigration
        diffs[:, colIndexer.priUnvacSusceptbIdx]  +=
            (S0u + S0v + S0i) * instantMigrationRate
        diffs[:, colIndexer.priUnvacInfectedIdxs] +=
            (I1u + I1v + I1i) * instantMigrationRate
        diffs[:, colIndexer.priUnvacRecovredIdxs] +=
            (R1u + R1v + R1i) * instantMigrationRate
        diffs[:, colIndexer.sndUnvacSusceptbIdxs] +=
            (S1u + S1v + S1i) * instantMigrationRate
        diffs[:, colIndexer.sndUnvacInfectedIdxs] +=
            (I2u + I2v + I2i) * instantMigrationRate
        diffs[:, colIndexer.cprUnvacInfectedIdxs] +=
            (IPu + IPv) * instantMigrationRate
        diffs[:, colIndexer.sndUnvacRecoverdIdx]  +=
            (R2u + R2v + R2i) * instantMigrationRate
    else
        diffs[:, 1 : colIndexer.nDiseaseStateColumns] +=
            sysState[:, 1 : colIndexer.nDiseaseStateColumns] *
            instantMigrationRate
    end

    # Additional quantities for aging & birth
    nOldestIndv = sum(sysState[nAgeGroups, 1 : colIndexer.nDiseaseStateColumns])
    nDeaths = nOldestIndv * seasonalBirthRate / vars.ageGroupSizes[nAgeGroups]
    nAdditionalBirths =
        instantNatPopGrowthRate * total_pop_size(sysState) * seasonalBirthRate
    nBirths = nDeaths + nAdditionalBirths

    nAddedByAging = vcat(
        transpose(zeros(Float64, colIndexer.nDiseaseStateColumns)),
        seasonalBirthRate *
            sysState[1 : end-1, 1 : colIndexer.nDiseaseStateColumns] ./
            vars.ageGroupSizes[1 : end-1]
    )
    nAddedByAging[1, colIndexer.priUnvacSusceptbIdx] = nBirths
    nRemovedByAging = seasonalBirthRate *
                      sysState[:, 1 : colIndexer.nDiseaseStateColumns] ./
                      vars.ageGroupSizes
    diffs[:, 1 : colIndexer.nDiseaseStateColumns] +=
        nAddedByAging - nRemovedByAging


    # ACCUMULATE THE NUMBER OF VACCINE SHOTS
    diffs[:, colIndexer.nNegShotsIdx] =
        S0u .* screeningCoverages * negVaccinatedProb
    diffs[:, colIndexer.nPriShotsIdx] =
        sum(R1u + S1u, dims = 2) .* screeningCoverages * priVaccinatedProb
    diffs[:, colIndexer.nSndShotsIdx] =
        R2u .* screeningCoverages * sndVaccinatedProb

end # function
