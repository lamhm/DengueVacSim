module Util

using StatsBase: sample, quantile
using DataFrames: DataFrame, rename!
using Dates


function print_time_stamp()
    currentTime = Dates.format(Dates.now(), "yyyy-mm-dd HH:MM:SS")
    println("╔═══════════════════════╗")
    println("║  ", currentTime, "  ║")
    println("╚═══════════════════════╝")
end



function row_quantile(dataMatrix::Matrix{T},
                      prob::Float64) :: Vector{Float64} where T<:Real
    nRows = size(dataMatrix, 1)
    results = Vector{Float64}(undef, nRows)
    for rowIdx = 1 : nRows
        results[rowIdx] = quantile(dataMatrix[rowIdx, :], prob)
    end
    return results
end



function row_quantile(dataMatrix::Matrix{T},
                      probs::Vector{Float64}) :: Matrix{Float64} where T<:Real
    nRows = size(dataMatrix, 1)
    nProbs = length(probs)
    results = Matrix{Float64}(undef, nRows, nProbs)
    for rowIdx = 1 : nRows
        results[rowIdx, :] = quantile(dataMatrix[rowIdx, :], probs)
    end
    return results
end



function row_quantile(dataMatrix::Matrix{T},
                      probs::Vector{Float64},
                      colNamePrefix::String;
                      nProbChars::Int = 3) :: DataFrame where T<:Real
    results = DataFrame( row_quantile(dataMatrix, probs) )

    nProbs = length(probs)
    colNames = Vector{Symbol}(undef, nProbs)
    for i = 1 : nProbs
        probString = string(probs[i])
        if probs[i] < 1.0
            probString = replace(probString, r"(\d*\.)"=>"")
        end
        while length(probString) < nProbChars
            probString *= '0'
        end # while
        colNames[i] = Symbol(colNamePrefix * probString)
    end
    rename!(results, colNames)

    return results
end



function group_matrix_columns(
            input::Matrix{T},
            nColumnsPerGroup::Int;
            maxConsecutiveZeros::Int = -1,
            missingMask::Real        = 0,
            rowBlocks::Vector{Vector{Int}} = Vector{Vector{Int}}(undef, 0)
            ) :: Matrix{T} where T<:Real

    nRows, nInputCols = size(input)
    nOutputCols = div(nInputCols, nColumnsPerGroup)

    if length(rowBlocks) <= 0
        rowBlocks = Vector{Vector{Int}}(undef, 1)
        rowBlocks[1] = [i for i = 1 : nRows]
    end

    output = zeros(T, nRows, nOutputCols)

    for outputCol = 1 : nOutputCols
        inputColRange = (
            (outputCol - 1) * nColumnsPerGroup + 1
            : outputCol * nColumnsPerGroup
        )

        for rowBlock in rowBlocks
            if length(rowBlock) <= 0
                continue
            end

            isDataMissing = false
            if 0 < maxConsecutiveZeros < nColumnsPerGroup
                # Try to detect if data is missing
                inputColSums = sum(input[rowBlock, inputColRange], dims = 1)
                nZerosFromLeft  = zeros(Int, nColumnsPerGroup)
                for i = 1 : nColumnsPerGroup
                    if inputColSums[i] <= 0
                        nZerosFromLeft[i] = 1
                        if i > 1
                            nZerosFromLeft[i] += nZerosFromLeft[i - 1]
                        end
                    end
                end
                nZerosFromRight = zeros(Int, nColumnsPerGroup)
                for i in nColumnsPerGroup : -1 : 1
                    if inputColSums[i] <= 0
                        nZerosFromRight[i] = 1
                        if i < nColumnsPerGroup
                            nZerosFromRight[i] += nZerosFromRight[i + 1]
                        end
                    end
                end
                nConsecutiveZeros = max(
                    maximum(nZerosFromLeft), maximum(nZerosFromRight)
                )
                if nConsecutiveZeros > maxConsecutiveZeros
                    isDataMissing = true
                end
            end

            if !isDataMissing
                output[rowBlock, outputCol] =
                    sum(input[rowBlock, inputColRange], dims = 2)
            else
                output[rowBlock, outputCol] =
                    [missingMask for i = 1 : length(rowBlock)]
            end
        end
    end

    return output
end # function group_matrix_columns



function zerofy_negative_numbers!(matrix::Matrix{T} where T<:Real)
    nRows, nCols = size(matrix)
    for row = 1 : nRows, col = 1 : nCols
        if matrix[row, col] < 0
            matrix[row, col] = 0
        end
    end
end



function avg_ages_of_groups(ageGroupSizes::Vector{Float64}) :: Vector{Float64}
    ageCutoffs = vcat( 0, cumsum(ageGroupSizes)[1:end-1] )
    return (ageGroupSizes ./ 2) + ageCutoffs
end



function moving_avg(series::Vector{T},
                   windowSize::Int,
                   movingWeight::Float64 = 1.0;
                   validateFunc::Function = !isnan)::Vector{Float64} where T<:Real
    seriesLen = length(series)
    result = Vector{Float64}(undef, seriesLen)
    halfSize = round(Int, (windowSize - 1.0) / 2.0, RoundNearestTiesUp)

    for i = 1 : seriesLen
        if !validateFunc(series[i])
            result[i] = series[i]
        else
            currentSum = series[i]
            nAvailElements = 1
            for k = 1 : halfSize
                currentWeight = movingWeight ^ k
                if (i + k <= seriesLen) && validateFunc(series[i + k])
                    currentSum += series[i + k] * currentWeight
                    nAvailElements += currentWeight
                end
                if (i - k >= 1) && validateFunc(series[i - k])
                    currentSum += series[i - k] * currentWeight
                    nAvailElements += currentWeight
                end
            end
            result[i] = Float64(currentSum) / nAvailElements
        end
    end

    return result
end # function movingAvg



function rand_samples(source::Vector, nOutcomes::Int) :: Vector
    sourceLength = length(source)

    if sourceLength == nOutcomes
        return deepcopy(source)
    elseif sourceLength > nOutcomes
        return sample(source, nOutcomes, replace = false)
    else
        return sample(source, nOutcomes, replace = true)
    end
end



function rand_samples(source::DataFrame, nOutcomes::Int) :: DataFrame
    nSourceRows = size(source, 1)
    selectedRows = rand_samples([i for i = 1 : nSourceRows], nOutcomes)
    return source[selectedRows, :]
end


end  # module Util
