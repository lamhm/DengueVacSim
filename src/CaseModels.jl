module CaseModels

import ..ModelConfigs: ModelVars, ModelParams



"""
Calculate the age-specific hospitalisation rate
This age-dependent function has a declining logistic shape.
"""
function age_specific_hosp_rates(ageGroupSizes::Vector{Float64},
                                 hospRateCutoffs::Vector{Float64},
                                 hospRates::Vector{Float64}
                                 ) :: Vector{Float64}

    nAgeGroups = length(ageGroupSizes)
    nHospRates = length(hospRateCutoffs)

    hospRatesOfAgeGroups = zeros(Float64, nAgeGroups)
    prevAge = 0.0
    ageGroupIdx = 0
    for hospRateIdx = 1 : nHospRates
        currentHospRate = hospRates[hospRateIdx]
        nextAgeCutoff = hospRateCutoffs[hospRateIdx]

        while ageGroupIdx < nAgeGroups &&
              (prevAge + ageGroupSizes[ageGroupIdx + 1]) <= nextAgeCutoff
            ageGroupIdx += 1
            hospRatesOfAgeGroups[ageGroupIdx] = currentHospRate
            prevAge += ageGroupSizes[ageGroupIdx]
        end
        if ageGroupIdx < nAgeGroups
            ageGroupIdx += 1
            newHospRate = hospRateIdx < nHospRates ?
                          hospRates[hospRateIdx + 1] : 0.0
            agePropInCurrHospRate = (nextAgeCutoff - prevAge) /
                                   ageGroupSizes[ageGroupIdx]
            hospRatesOfAgeGroups[ageGroupIdx] =
                agePropInCurrHospRate * currentHospRate +
                (1.0 - agePropInCurrHospRate) * newHospRate
            prevAge += ageGroupSizes[ageGroupIdx]
        else
            break
        end
    end
    return hospRatesOfAgeGroups
end # function



age_specific_hosp_rates(vars::ModelVars, params::ModelParams) =
    age_specific_hosp_rates(vars.ageGroupSizes, vars.hospRateCutoffs,
                            params.hospRates)



"""
    maternal_effects(ageGroupSizes::Vector{Float64},
                     maternalProtectDuration::T,
                     adeAgeAvg::T,
                     adeAgeVar::T,
                     adeEffectScale::T,
                     nBinsPerAgeGroup::Int = 10) :: Vector{T}

Calculate the multiplicative age-dependent effect of the maternal immunity in
causing symptoms during a primary infection. This effect include 2 parts:
(1) When `age <= maternalProtectDuration`, the maternal immunity is fully
    protective, i.e. the effect scale is 0.
(2) When `age > maternalProtectDuration`, the maternal immunity can enhance
    infection. The enhancement effect (ADE) is modelled as a gaussian-shaped
    function, of which the maximum value is `adeEffectScale` and the minimum
    value is 1.0. This function reaches its maximum value when
    `age == aedAgeAvg`.

The ADE part is modelled by the following function:
    ADE(age) = exp( -((age - adeAgeAvg) ^ 2) / (2 * adeAgeVar) ) *
               (adeEffectScale - 1.0) + 1.0
"""
function maternal_effects(ageGroupSizes::Vector{Float64},
                          maternalProtectDuration::Float64,
                          adeAgeAvg::Float64,
                          adeAgeVar::Float64,
                          adeEffectScale::Float64,
                          nBinsPerAgeGroup::Int = 10
                          ) :: Vector{Float64}

    LOG_MINIMUM = -32  # ≈ ln(1e-14)

    nAgeGroups = length(ageGroupSizes)
    binSizes = ageGroupSizes / (nBinsPerAgeGroup + 1.0)
    age = 0.0
    maternalEffects = zeros(Float64, nAgeGroups)
    for ageGroup = 1 : nAgeGroups
        age += binSizes[ageGroup]

        for bin = 1 : nBinsPerAgeGroup
            if age > maternalProtectDuration
                if adeEffectScale <= 1.0
                    maternalEffects[ageGroup] += 1.0
                else
                    logEffect = -((age - adeAgeAvg) ^ 2) / (2 * adeAgeVar)
                    if logEffect > LOG_MINIMUM
                        maternalEffects[ageGroup] +=
                            exp(logEffect) * (adeEffectScale - 1.0) + 1.0
                    else
                        maternalEffects[ageGroup] += 1.0
                    end
                end
            end
            age += binSizes[ageGroup]
        end
    end
    maternalEffects ./= nBinsPerAgeGroup

    return maternalEffects
end # function



maternal_effects(vars::ModelVars,
                 params::ModelParams,
                 nBinsPerAgeGroup::Int = 10) =
    maternal_effects(vars.ageGroupSizes,
                     params.maternalProtectDuration,
                     params.adeAgeAvg,
                     params.adeAgeVar,
                     params.adeEffectScale,
                     nBinsPerAgeGroup)





maternal_protective_effects(vars::ModelVars, params::ModelParams) =
    maternal_protective_effects(vars.ageGroupSizes,
                                params.maternalProtectDuration)






end  # module CaseModels
