"""
The ODE system of the SIR model. This function should only by used by the ODE
solver.
"""
function ode_system_vaccinate_via_aging(
                diffs::Matrix{Float64},
                sysState::Matrix{Float64},
                modelConfig::Tuple{ModelVars, ModelParams},
                odeTime::Float64)

    vars, params = modelConfig

    nSerotypes = vars.nSerotypes
    nAgeGroups = length(vars.ageGroupSizes)

    # Calculate seasonal birth rate
    odeYearIdx, odeDayOfYear = intepret_ode_time(odeTime, vars)
    birthSeasonLength = vars.nDaysPerYear / length(vars.seasonalBirthRates)
    birthSeasonIdx    = Int(ceil(odeDayOfYear / birthSeasonLength))
    seasonalBirthRate = length(vars.seasonalBirthRates) *
                        vars.seasonalBirthRates[birthSeasonIdx] /
                        sum(vars.seasonalBirthRates)

    # Calculate population growth
    instantNatPopGrowthRate = 0.0
    instantMigrationRate    = 0.0
    if odeYearIdx >= 0 && odeYearIdx < vars.nSimYears
        instantNatPopGrowthRate = convert_annual_rate_to_daily_rate(
            vars.annualNatPopGrowthRates[odeYearIdx + 1] - 1.0,
            vars.nDaysPerYear
        )
        instantMigrationRate = convert_annual_rate_to_daily_rate(
            vars.annualNetMigRates[odeYearIdx + 1],
            vars.nDaysPerYear
        )

    elseif length(vars.annualNatPopGrowthRates) > 0
        instantNatPopGrowthRate = convert_annual_rate_to_daily_rate(
            minimum(vars.annualNatPopGrowthRates) - 1.0,
            vars.nDaysPerYear
        )
    end

    # Retrieve vaccine coverage
    screeningCoverages = params.screeningCoverages
    if (odeYearIdx < vars.simYearToStartVaccination) ||
            (vars.simYearToStartVaccination < 0)
        screeningCoverages = zeros(Float64, nAgeGroups)
    end

    # Other vaccine-related parameters
    vaccineResponseProb = params.vaccineResponseProb
    negVacProbs = screeningCoverages * params.negVaccinatedProb
    priVacProbs = screeningCoverages * params.priVaccinatedProb
    sndVacProbs = screeningCoverages * params.sndVaccinatedProb

    Util.zerofy_negative_numbers!(sysState)
    colIndexer = SIRColumnIndexer(nSerotypes)

    S0u = sysState[:, colIndexer.priUnvacSusceptbIdx]
    S0v = sysState[:, colIndexer.priVacciSusceptbIdx]
    S0i = sysState[:, colIndexer.priImmunSusceptbIdx]

    I1u = sysState[:, colIndexer.priUnvacInfectedIdxs]
    I1v = sysState[:, colIndexer.priVacciInfectedIdxs]
    I1i = sysState[:, colIndexer.priImmunInfectedIdxs]

    R1u = sysState[:, colIndexer.priUnvacRecovredIdxs]
    R1v = sysState[:, colIndexer.priVacciRecovredIdxs]
    R1i = sysState[:, colIndexer.priImmunRecovredIdxs]

    S1u = sysState[:, colIndexer.sndUnvacSusceptbIdxs]
    S1v = sysState[:, colIndexer.sndVacciSusceptbIdxs]
    S1i = sysState[:, colIndexer.sndImmunSusceptbIdxs]

    I2u = sysState[:, colIndexer.sndUnvacInfectedIdxs]
    I2v = sysState[:, colIndexer.sndVacciInfectedIdxs]
    I2i = sysState[:, colIndexer.sndImmunInfectedIdxs]

    IPu = sysState[:, colIndexer.cprUnvacInfectedIdxs]
    IPv = sysState[:, colIndexer.cprVacciInfectedIdxs]

    R2u = sysState[:, colIndexer.sndUnvacRecoverdIdx]
    R2v = sysState[:, colIndexer.sndVacciRecoverdIdx]
    R2i = sysState[:, colIndexer.sndImmunRecoverdIdx]

    # Calculate primary and secondary FOIs
    priFois = pri_fois(sysState, vars, params, odeTime)
    totalPriFoi = sum(priFois)
    totalSndFoi = totalPriFoi .- priFois
    sndFois = repeat(transpose(priFois), nSerotypes, 1)
    sndFois[1 : (nSerotypes + 1) : end] .= 0  # make the diagonal become zeros


    # PRIMARY SUSCEPTIBLE (without aging, migration, and vaccination)
    # The amount of S0u removed by infection
    diffs[:, colIndexer.priUnvacSusceptbIdx] = -S0u * totalPriFoi

    # The amount of S0v removed by infection
    diffs[:, colIndexer.priVacciSusceptbIdx] = -S0v * totalPriFoi

    # The amount of S0i removed by infection
    diffs[:, colIndexer.priImmunSusceptbIdx] = -S0i * totalPriFoi


    # PRIMARY INFECTED (without aging, migration, and vaccination)
    # Derivatives of I1u
    nAddedByInfection  = S0u * transpose(priFois)
    nRemovedByRecovery = I1u * params.recoveryRate
    diffs[:, colIndexer.priUnvacInfectedIdxs] =
            nAddedByInfection - nRemovedByRecovery

    # Derivatives of I1v
    nAddedByInfection  = S0v * transpose(priFois)
    nRemovedByRecovery = I1v * params.recoveryRate
    diffs[:, colIndexer.priVacciInfectedIdxs] =
            nAddedByInfection - nRemovedByRecovery

    # Derivatives of I1i
    nAddedByInfection  = S0i * transpose(priFois)
    nRemovedByRecovery = I1i * params.recoveryRate
    diffs[:, colIndexer.priImmunInfectedIdxs] =
            nAddedByInfection - nRemovedByRecovery


    # PRIMARY RECOVERED (without aging, migration, and vaccination)
    # Derivatives of R1u
    nAddedByRecovery = I1u * params.recoveryRate
    nRemovedByImmWaning = R1u / params.crossProtectDuration
    nRemovedByInfection =
            R1u .* transpose(totalSndFoi) * params.infectRiskDuringPriCpr
    diffs[:, colIndexer.priUnvacRecovredIdxs] =
            nAddedByRecovery - nRemovedByImmWaning - nRemovedByInfection

    # Derivatives of R1v
    nAddedByRecovery = I1v * params.recoveryRate
    nRemovedByImmWaning = R1v / params.crossProtectDuration
    nRemovedByInfection =
            R1v .* transpose(totalSndFoi) * params.infectRiskDuringPriCpr
    diffs[:, colIndexer.priVacciRecovredIdxs] =
            nAddedByRecovery - nRemovedByImmWaning - nRemovedByInfection

    # Derivatives of R1i
    nAddedByRecovery = I1i * params.recoveryRate
    nRemovedByImmWaning = R1i / params.crossProtectDuration
    nRemovedByInfection =
            R1i .* transpose(totalSndFoi) * params.infectRiskDuringPriCpr
    diffs[:, colIndexer.priImmunRecovredIdxs] =
            nAddedByRecovery - nRemovedByImmWaning - nRemovedByInfection


    # SECONDARY SUSCEPTIBLE (without aging, migration, and vaccination)
    # Derivatives of S1u
    nAddedByImmWaning  = R1u / params.crossProtectDuration
    nRemovedByInfection = S1u .* transpose(totalSndFoi)
    diffs[:, colIndexer.sndUnvacSusceptbIdxs] =
            nAddedByImmWaning - nRemovedByInfection

    # Derivatives of S1v
    nAddedByImmWaning  = R1v / params.crossProtectDuration
    nRemovedByInfection = S1v .* transpose(totalSndFoi)
    diffs[:, colIndexer.sndVacciSusceptbIdxs] =
            nAddedByImmWaning - nRemovedByInfection

    # Derivatives of S1i
    nAddedByImmWaning  = R1i / params.crossProtectDuration
    nRemovedByInfection = S1i .* transpose(totalSndFoi)
    diffs[:, colIndexer.sndImmunSusceptbIdxs] =
            nAddedByImmWaning - nRemovedByInfection


    # SECONDARY INFECTED (without aging, migration, and vaccination)
    # Derivatives of I2u
    nAddedByInfection  = S1u * sndFois
    nRemovedByRecovery = I2u * params.recoveryRate
    diffs[:, colIndexer.sndUnvacInfectedIdxs] =
            nAddedByInfection - nRemovedByRecovery

    # Derivatives of I2v
    nAddedByInfection  = S1v * sndFois
    nRemovedByRecovery = I2v * params.recoveryRate
    diffs[:, colIndexer.sndVacciInfectedIdxs] =
            nAddedByInfection - nRemovedByRecovery

    # Derivatives of I2i
    nInfectDuringPriCpr = params.infectRiskDuringPriCpr * R1i * sndFois
    nInfectDuringSusctb = S1i * sndFois
    nRemovedByRecovery = I2i * params.recoveryRate
    diffs[:, colIndexer.sndImmunInfectedIdxs] =
            nInfectDuringPriCpr + nInfectDuringSusctb - nRemovedByRecovery


    # INFECTED DURING PRIMARY CROSS-PROTECTION
    # (without aging, migration, and vaccination)
    # Derivatives of IPu
    nAddedByInfection = params.infectRiskDuringPriCpr * R1u * sndFois
    nRemovedByRecovery = IPu * params.recoveryRate
    diffs[:, colIndexer.cprUnvacInfectedIdxs] =
            nAddedByInfection - nRemovedByRecovery

    # Derivatives of IPv
    nAddedByInfection = params.infectRiskDuringPriCpr * R1v * sndFois
    nRemovedByRecovery = IPv * params.recoveryRate
    diffs[:, colIndexer.cprVacciInfectedIdxs] =
            nAddedByInfection - nRemovedByRecovery


    # SECONDARY RECOVERED (without aging, migration, and vaccination)
    # The number of new recovereds added to R2u
    diffs[:, colIndexer.sndUnvacRecoverdIdx] =
            sum((IPu + I2u), dims = 2) * params.recoveryRate

    # The number of new recovereds added to R2v
    diffs[:, colIndexer.sndVacciRecoverdIdx] =
            sum((IPv + I2v), dims = 2) * params.recoveryRate

    # The number of new recovereds added to R2i
    diffs[:, colIndexer.sndImmunRecoverdIdx] =
            sum(I2i, dims = 2) * params.recoveryRate


    # ADDITIONAL QUANTITIES FOR IMMIGRATION
    if vars.noVacBeforeImmigration
        diffs[:, colIndexer.priUnvacSusceptbIdx]  +=
            (S0u + S0v + S0i) * instantMigrationRate
        diffs[:, colIndexer.priUnvacInfectedIdxs] +=
            (I1u + I1v + I1i) * instantMigrationRate
        diffs[:, colIndexer.priUnvacRecovredIdxs] +=
            (R1u + R1v + R1i) * instantMigrationRate
        diffs[:, colIndexer.sndUnvacSusceptbIdxs] +=
            (S1u + S1v + S1i) * instantMigrationRate
        diffs[:, colIndexer.sndUnvacInfectedIdxs] +=
            (I2u + I2v + I2i) * instantMigrationRate
        diffs[:, colIndexer.cprUnvacInfectedIdxs] +=
            (IPu + IPv) * instantMigrationRate
        diffs[:, colIndexer.sndUnvacRecoverdIdx]  +=
            (R2u + R2v + R2i) * instantMigrationRate
    else
        diffs[:, 1 : colIndexer.nDiseaseStateColumns] +=
            sysState[:, 1 : colIndexer.nDiseaseStateColumns] *
            instantMigrationRate
    end


    # ADDITIONAL QUANTITIES FOR AGING & VACCINATION
    # Calculate the aging rate of each age group
    agingRates = seasonalBirthRate ./ vars.ageGroupSizes

    # Calculate the number of new births
    nOldestIndv = sum(sysState[nAgeGroups, 1 : colIndexer.nDiseaseStateColumns])
    nDeaths = nOldestIndv * agingRates[nAgeGroups]
    nAdditionalBirths =
        instantNatPopGrowthRate * total_pop_size(sysState) * seasonalBirthRate
    nBirths = nDeaths + nAdditionalBirths

    # Outcoming aging
    nRemovedByAging =
            sysState[:, 1 : colIndexer.nDiseaseStateColumns] .* agingRates

    # Incoming aging
    nAddedByAging = vcat(
            transpose(zeros(Float64, colIndexer.nDiseaseStateColumns)),
            sysState[1 : end-1, 1 : colIndexer.nDiseaseStateColumns] .*
                agingRates[1 : end-1]
    )
    nAddedByAging[1, colIndexer.priUnvacSusceptbIdx] = nBirths

    # Calculate the number of vaccines
    nVaccinesOnPriSusc =
            nAddedByAging[:, colIndexer.priUnvacSusceptbIdx] .* negVacProbs
    nVaccinesOnPriReco =
            nAddedByAging[:, colIndexer.priUnvacRecovredIdxs] .* priVacProbs
    nVaccinesOnSndSusc =
            nAddedByAging[:, colIndexer.sndUnvacSusceptbIdxs] .* priVacProbs
    nVaccinesOnSndReco =
            nAddedByAging[:, colIndexer.sndUnvacRecoverdIdx] .* sndVacProbs

    # Incorporate vaccination into incoming aging
    nAddedByAging[:, colIndexer.priUnvacSusceptbIdx] -= nVaccinesOnPriSusc
    nAddedByAging[:, colIndexer.priVacciSusceptbIdx] +=
            nVaccinesOnPriSusc * (1.0 - vaccineResponseProb)
    nAddedByAging[:, colIndexer.priImmunSusceptbIdx] +=
            nVaccinesOnPriSusc * vaccineResponseProb

    nAddedByAging[:, colIndexer.priUnvacRecovredIdxs] -= nVaccinesOnPriReco
    nAddedByAging[:, colIndexer.priVacciRecovredIdxs] +=
            nVaccinesOnPriReco * (1.0 - vaccineResponseProb)
    nAddedByAging[:, colIndexer.priImmunRecovredIdxs] +=
            nVaccinesOnPriReco * vaccineResponseProb

    nAddedByAging[:, colIndexer.sndUnvacSusceptbIdxs] -= nVaccinesOnSndSusc
    nAddedByAging[:, colIndexer.sndVacciSusceptbIdxs] +=
            nVaccinesOnSndSusc * (1.0 - vaccineResponseProb)
    nAddedByAging[:, colIndexer.sndImmunSusceptbIdxs] +=
            nVaccinesOnSndSusc * vaccineResponseProb

    nAddedByAging[:, colIndexer.sndUnvacRecoverdIdx] -= nVaccinesOnSndReco
    nAddedByAging[:, colIndexer.sndVacciRecoverdIdx] +=
            nVaccinesOnSndReco * (1.0 - vaccineResponseProb)
    nAddedByAging[:, colIndexer.sndImmunRecoverdIdx] +=
            nVaccinesOnSndReco * vaccineResponseProb

    # Update the derivatives
    diffs[:, 1 : colIndexer.nDiseaseStateColumns] +=
        nAddedByAging - nRemovedByAging


    # ACCUMULATE THE NUMBER OF VACCINE SHOTS
    diffs[:, colIndexer.nNegShotsIdx] = nVaccinesOnPriSusc
    diffs[:, colIndexer.nPriShotsIdx] =
        sum(nVaccinesOnPriReco + nVaccinesOnSndSusc, dims = 2)
    diffs[:, colIndexer.nSndShotsIdx] = nVaccinesOnSndReco

end # function
