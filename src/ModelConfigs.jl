module ModelConfigs

import YAML
import DataFrames


"List of all model variables"
mutable struct ModelVars
    ageGroupSizes::Vector{Float64}

    nDaysPerYear::Float64
    nDaysPerSummaryPeriod::Int
    nDaysReportingLag::Float64

    nSimYears::Float64

    """
    The simulated year that the vaccination start.
    The first simulated year is indexed as zero.
    If this value is set to negative, vaccination will be disable.
    """
    simYearToStartVaccination::Float64

    """
    The following flag indicates whether immigrants originate from a completely
    unvaccinated population or not.
    """
    noVacBeforeImmigration::Bool

    initPopSize::Float64

    """
    Annual natural population growth rates.
    Natural growth rate = 1.0 means number of births = number of deaths.
    N.B. For almost every area of Vietnam, these rates have been always >1.
    """
    annualNatPopGrowthRates::Vector{Float64}

    """
    Annual net migration rates:
        = 0.0 : immigrations and emigrations were equal.
        < 0.0 : immigrations < emigrations
        > 0.0 : immigrations > emigrations
    """
    annualNetMigRates::Vector{Float64}

    seasonalBirthRates::Vector{Float64}

    nSerotypes::Int
    nSeasonalTranPeriods::Int

    "Age cut-offs for age-specific hospitalisation rates"
    hospRateCutoffs::Vector{Float64}

    nOdeWarmupYears::Int
    odeAbsTolerance::Float64
    odeRelTolerance::Float64
end # ModelVars
export ModelVars


function validate!(vars::ModelVars)
    if vars.nSerotypes < 1 || vars.nSerotypes > 4
        throw(ErrorException(
            "the number of serotypes must be between 1 and 4."
        ))
    end

    sumSeasonalBirthRates = sum(vars.seasonalBirthRates)
    if sumSeasonalBirthRates != 1.0
        for i in 1 : length(vars.seasonalBirthRates)
            vars.seasonalBirthRates[i] /= sumSeasonalBirthRates
        end
    end

    if vars.nSimYears > length(vars.annualNatPopGrowthRates)
        throw(DomainError(
            vars.nSimYears,
            "not enough data for the natural population growth rate " *
            "(required: $(vars.nSimYears) years, " *
            "received: $(length(vars.annualNatPopGrowthRates)) years)."
        ))
    end

    if vars.nSimYears > length(vars.annualNetMigRates)
        throw(DomainError(
            vars.nSimYears,
            "not enough data for the net migration rate " *
            "(required: $(vars.nSimYears) years, " *
            "received: $(length(vars.annualNetMigRates)) years)."
        ))
    end

    if !all(0.9 .<= vars.annualNatPopGrowthRates .<= 1.1)
        throw(DomainError(
            vars.annualNatPopGrowthRates,
            "the annual natural population growth rates are not in the " *
            "expected range, i.e. [0.9, 1.1]."
        ))
    end

    if !all(-0.1 .<= vars.annualNetMigRates .<= 0.1)
        throw(DomainError(
            vars.annualNetMigRates,
            "the annual net migration rates are not in the expected " *
            "range, i.e. [-0.1, 0.1]."
        ))
    end
end # function


"""
List of all model parameters. Technically, the values of these parameters can be
altered during the model fitting process.
"""
mutable struct ModelParams
    serotypeTranRates::Vector{Float64}
    seasonalTranScales::Vector{Float64}

    recoveryRate::Float64

    "The waning rate of the cross-immunity induced by a primary infection"
    crossProtectDuration::Float64

    """
    The risk of infection during the primary cross-protection period.
    This parameter ranges between 0 and 1.
    """
    infectRiskDuringPriCpr::Float64

    hospRates::Vector{Float64}

    adeEffectScale::Float64
    adeAgeAvg::Float64
    adeAgeVar::Float64

    maternalProtectDuration::Float64

    sndSymptomaticProb::Float64
    priSymptomaticProb::Float64

    " The coverage of screening for each age group"
    screeningCoverages::Vector{Float64}

    """
    Probability that a vaccine recipient develop immune response
    (which can be either protective or harmful).
    """
    vaccineResponseProb::Float64

    """
    Probability that the vaccine creates a transmission-blocking effect on
    effectively vaccinated recipients during their secondary infections.
    """
    transBlockingEfficacy::Float64

    negVaccinatedProb::Float64
    priVaccinatedProb::Float64
    sndVaccinatedProb::Float64

end # struct
export ModelParams



function validate!(params::ModelParams, vars::ModelVars)
    if params.seasonalTranScales[1] != 1.0
        params.seasonalTranScales ./= params.seasonalTranScales[1]
    end

    if any(params.seasonalTranScales .<= 0.0)
        throw(DomainError(
            params.seasonalTranScales,
            "seasonal transmission scales must be positive."
        ))
    end

    if params.infectRiskDuringPriCpr < 0.0 || params.infectRiskDuringPriCpr > 1.0
        throw(DomainError(
            params.infectRiskDuringPriCpr,
            "the risk of infection during the primary cross-protection period "
            * " must be in [0, 1] range."
        ))
    end

    if vars.nSerotypes != length(params.serotypeTranRates)
        throw(ErrorException(
            "the number of serotypes does not match the number of " *
            "serotype-specific transmisison rates."
        ))
    end

    if vars.nSeasonalTranPeriods != length(params.seasonalTranScales)
        throw(ErrorException(
            "the number of transmission periods per year does not match the " *
            "number of period-specific transmisison scales."
        ))
    end

    if length(vars.hospRateCutoffs) != length(params.hospRates)
        throw(ErrorException(
            "the number of age cut-offs for hospitalisation rates does not " *
            "match the number of age-specific hospitalisation rates."
        ))
    end

end # function



function set_params_for_no_vaccine!(params::ModelParams)
    params.negVaccinatedProb = 0.0
    params.priVaccinatedProb = 0.0
    params.sndVaccinatedProb = 0.0
end



function set_params_for_no_screen!(params::ModelParams)
    params.negVaccinatedProb = 1.0
    params.priVaccinatedProb = 1.0
    params.sndVaccinatedProb = 1.0
end



function set_params_for_perfect_screen!(params::ModelParams)
    params.negVaccinatedProb = 0.0
    params.priVaccinatedProb = 1.0
    params.sndVaccinatedProb = 1.0
end



function set_vaccinated_probs!(params::ModelParams,
                               negVaccinatedProb::Real,
                               priVaccinatedProb::Real,
                               sndVaccinatedProb::Real)
    params.negVaccinatedProb = Float64(negVaccinatedProb)
    params.priVaccinatedProb = Float64(priVaccinatedProb)
    params.sndVaccinatedProb = Float64(sndVaccinatedProb)
end



"""
    parse_yml_data(outType::Type, ymlData::Dict)

Parse YAML data (stored in a `Dict` object) into a given output type.
"""
function parse_yml_data(outType::Type, ymlData::Dict)
    # Helper functions
    format_data(type::Type{T}, value::Number) where T<:Number =
        convert(type, value)
    format_data(type::Type{String}, value) = string(value)
    format_data(type::Type{T}, value::String) where T<:Number =
        format_data(type, eval(Meta.parse(value)))

    # The parsing process starts here
    objData = Vector{Any}()
    for field in fieldnames(outType)
        fieldType = fieldtype(outType, field)
        fieldName = string(field)
        if !(fieldName in keys(ymlData))
            throw(ErrorException(
                "'$(fieldName)' is not found in the configuration data for " *
                "'$(outType)'."
            ))
        end
        ymlDataOfField = ymlData[fieldName]

        if fieldType <: Vector
            elementType = eltype(fieldType)
            fieldData = Vector{elementType}()
            for elementData in ymlDataOfField
                formattedData = format_data(elementType, elementData)
                push!(fieldData, formattedData)
            end
        elseif fieldType <: Number || fieldType <: String
            fieldData = format_data(fieldType, ymlDataOfField)
        elseif isstructtype(fieldType)
            fieldData = parse_yml_data(fieldType, ymlDataOfField)
        end
        push!(objData, fieldData)
    end
    return (eval(outType))(objData...)
end # function



"""
    load_model_config(ymlFile::String) :: ModelConfig

Load model configuration from a YAML file.
"""
function load_model_config(ymlFile::String) :: Tuple{ModelVars, ModelParams}
    configData = YAML.load_file(ymlFile)

    modelVars = parse_yml_data(ModelVars, configData["vars"])
    modelParams = parse_yml_data(ModelParams, configData["params"])

    validate!(modelVars)
    validate!(modelParams, modelVars)

    return (modelVars, modelParams)
end # function
export load_model_config



function update_params!(params::ModelParams, vars::ModelVars,
                        paramValuesDf::DataFrames.DataFrame, selectedRow::Int)
    params.serotypeTranRates =
        [paramValuesDf[selectedRow, :primaryTranRate] for i = 1 : vars.nSerotypes]

    for i = 1 : vars.nSeasonalTranPeriods
        params.seasonalTranScales[i] =
            paramValuesDf[selectedRow, Symbol("seasonalTranScales_" * string(i))]
    end

    params.crossProtectDuration =
        paramValuesDf[selectedRow, :crossProtectDuration]

    nHospRates = length(vars.hospRateCutoffs)
    for i in 1 : nHospRates
        params.hospRates[i] =
            paramValuesDf[selectedRow, Symbol("hospRates_" * string(i))]
    end

    params.adeEffectScale = paramValuesDf[selectedRow, :adeEffectScale]
    params.adeAgeAvg      = paramValuesDf[selectedRow, :adeAgeAvg]
    params.adeAgeVar      = paramValuesDf[selectedRow, :adeAgeVar]
    params.maternalProtectDuration =
        paramValuesDf[selectedRow, :maternalProtectDuration]

    params.sndSymptomaticProb = paramValuesDf[selectedRow, :sndSymptomaticProb]
    params.priSymptomaticProb = paramValuesDf[selectedRow, :priSymptomaticProb]

    # infectRiskDuringPriCpr::Float64
    # screeningCoverages::Vector{Float64}
    # vaccineResponseProb::Float64
    # transBlockingEfficacy::Float64
    # negVaccinatedProb::Float64
    # priVaccinatedProb::Float64
    # sndVaccinatedProb::Float64
end



end  # module ModelConfigs
