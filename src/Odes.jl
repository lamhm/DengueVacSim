module Odes

import ..Util
import ..ModelConfigs: ModelVars, ModelParams

using DifferentialEquations


include("Odes_system_horizontal.jl")
include("Odes_system_vertical.jl")



struct SIRColumnIndexer
    "Total number of columns."
    nColumns::Int

    nDiseaseStateColumns::Int

    "Column index of the primary susceptible groups."
    priUnvacSusceptbIdx::Int  # unvaccinated
    priVacciSusceptbIdx::Int  # vaccinated without immune response
    priImmunSusceptbIdx::Int  # vaccinated with immune response

    """
    Column indices of the primary infected groups.
    Each index corresponds to a serotype.
    """
    priUnvacInfectedIdxs::Vector{Int}
    priVacciInfectedIdxs::Vector{Int}
    priImmunInfectedIdxs::Vector{Int}

    """
    Column indices of the primary recovered groups.
    Each index corresponds to a serotype.
    """
    priUnvacRecovredIdxs::Vector{Int}
    priVacciRecovredIdxs::Vector{Int}
    priImmunRecovredIdxs::Vector{Int}

    """
    Column indices of the secondary susceptible groups.
    Each index corresponds to a serotype.
    """
    sndUnvacSusceptbIdxs::Vector{Int}
    sndVacciSusceptbIdxs::Vector{Int}
    sndImmunSusceptbIdxs::Vector{Int}

    """
    Column indices of the seconary infected groups.
    Each index corresponds to a serotype.
    """
    sndUnvacInfectedIdxs::Vector{Int}
    sndVacciInfectedIdxs::Vector{Int}
    sndImmunInfectedIdxs::Vector{Int}

    """
    Column indices of the population groups that are secondary infected while
    they are still cross-protected (cpr) from symptoms.
    Each index corresponds to a serotype.
    """
    cprUnvacInfectedIdxs::Vector{Int}
    cprVacciInfectedIdxs::Vector{Int}

    "Column index of the secondary recovered group."
    sndUnvacRecoverdIdx::Int
    sndVacciRecoverdIdx::Int
    sndImmunRecoverdIdx::Int

    "Column indices for the cumulative numbers of vaccine shots."
    nNegShotsIdx::Int
    nPriShotsIdx::Int
    nSndShotsIdx::Int


    function SIRColumnIndexer(nSerotypes::Int)
        nColumns = 0

        priUnvacSusceptbIdx = (nColumns += 1)
        priVacciSusceptbIdx = (nColumns += 1)
        priImmunSusceptbIdx = (nColumns += 1)

        priUnvacInfectedIdxs = [nColumns += 1 for i = 1:nSerotypes]
        priVacciInfectedIdxs = [nColumns += 1 for i = 1:nSerotypes]
        priImmunInfectedIdxs = [nColumns += 1 for i = 1:nSerotypes]

        priUnvacRecovredIdxs = [nColumns += 1 for i = 1:nSerotypes]
        priVacciRecovredIdxs = [nColumns += 1 for i = 1:nSerotypes]
        priImmunRecovredIdxs = [nColumns += 1 for i = 1:nSerotypes]

        sndUnvacSusceptbIdxs = [nColumns += 1 for i = 1:nSerotypes]
        sndVacciSusceptbIdxs = [nColumns += 1 for i = 1:nSerotypes]
        sndImmunSusceptbIdxs = [nColumns += 1 for i = 1:nSerotypes]

        sndUnvacInfectedIdxs = [nColumns += 1 for i = 1:nSerotypes]
        sndVacciInfectedIdxs = [nColumns += 1 for i = 1:nSerotypes]
        sndImmunInfectedIdxs = [nColumns += 1 for i = 1:nSerotypes]

        cprUnvacInfectedIdxs = [nColumns += 1 for i = 1:nSerotypes]
        cprVacciInfectedIdxs = [nColumns += 1 for i = 1:nSerotypes]

        sndUnvacRecoverdIdx = (nColumns += 1)
        sndVacciRecoverdIdx = (nColumns += 1)
        sndImmunRecoverdIdx = (nColumns += 1)

        nDiseaseStateColumns = nColumns

        nNegShotsIdx = (nColumns += 1)
        nPriShotsIdx = (nColumns += 1)
        nSndShotsIdx = (nColumns += 1)

        new( nColumns,
             nDiseaseStateColumns,

             priUnvacSusceptbIdx,
             priVacciSusceptbIdx,
             priImmunSusceptbIdx,

             priUnvacInfectedIdxs,
             priVacciInfectedIdxs,
             priImmunInfectedIdxs,

             priUnvacRecovredIdxs,
             priVacciRecovredIdxs,
             priImmunRecovredIdxs,

             sndUnvacSusceptbIdxs,
             sndVacciSusceptbIdxs,
             sndImmunSusceptbIdxs,

             sndUnvacInfectedIdxs,
             sndVacciInfectedIdxs,
             sndImmunInfectedIdxs,

             cprUnvacInfectedIdxs,
             cprVacciInfectedIdxs,

             sndUnvacRecoverdIdx,
             sndVacciRecoverdIdx,
             sndImmunRecoverdIdx,

             nNegShotsIdx,
             nPriShotsIdx,
             nSndShotsIdx )
    end
end # struct



"""
Generate the initial conditions for the ODE solver based on the given model
conditions.
"""
function zeroth_sys_state(modelVars::ModelVars;
                          totalTranRate::Float64 = 1.6,
                          recoveryRate::Float64  = 1.0 / 7.0,
                          crossProtectDuration::Float64 = 120.0
                          ) :: Matrix{Float64}

    nSerotypes = modelVars.nSerotypes
    serotypeTranRates = [totalTranRate / nSerotypes for i = 1 : nSerotypes]

    totalPopSize = modelVars.initPopSize

    nAgeGroups = length(modelVars.ageGroupSizes)
    maxSimAge = sum(modelVars.ageGroupSizes)

    proportionOfAgeGroups =
        [ageGroupSize / maxSimAge for ageGroupSize = modelVars.ageGroupSizes]
    popSizeOfAgeGroups = proportionOfAgeGroups * totalPopSize
    unassignedPops = popSizeOfAgeGroups

    #=
    Approximated force of infection:
    FOI = I * tranRate
        ≈ tranRate * (deathRate / (recoveryRate + deathRate)) - death_rate
    =#
    deathRate  = 1.0 / maxSimAge
    priFois = serotypeTranRates *
           (deathRate / (recoveryRate + deathRate)) .- deathRate
    for i = 1 : nSerotypes
        if priFois[i] < 0.0
            priFois[i] = 0.0
        end
    end

    totalPriFoi = sum(priFois)
    if totalPriFoi <= 0.0
        throw(
            DomainError(totalTranRate,
                        "transmission rate is too low to sustain epidemics.")
        )
    end

    nInfectedPerSerotype = priFois ./ serotypeTranRates * totalPopSize
    nInfectedTotal = sum(nInfectedPerSerotype)
    serotypeProportions = nInfectedPerSerotype / nInfectedTotal


    colIndexer = SIRColumnIndexer(nSerotypes)
    sysState = zeros(Float64, nAgeGroups, colIndexer.nColumns)

    avgAgeOfGroups = Util.avg_ages_of_groups(modelVars.ageGroupSizes)

    # Su₀: unvaccinated primary susceptible
    nPriUnvacSusceptb = popSizeOfAgeGroups .* exp.(-totalPriFoi * avgAgeOfGroups)
    unassignedPops -= nPriUnvacSusceptb
    sysState[:, colIndexer.priUnvacSusceptbIdx] = nPriUnvacSusceptb

    # Iu₁: unvaccinated primary infected
    nUnassignedInfected = nInfectedPerSerotype
    for ageGroup = 1 : nAgeGroups
        if ageGroup == nAgeGroups
            nExpectedInfected = sum(nUnassignedInfected)
        else
            nExpectedInfected = nInfectedTotal * proportionOfAgeGroups[ageGroup]
        end
        if nExpectedInfected < 0
            nExpectedInfected = 0
        elseif nExpectedInfected > unassignedPops[ageGroup]
            nExpectedInfected = unassignedPops[ageGroup]
        end

        unassignedPops[ageGroup] -= nExpectedInfected
        nPriUnvacInfected = nExpectedInfected * serotypeProportions
        nUnassignedInfected -= nPriUnvacInfected
        sysState[ageGroup, colIndexer.priUnvacInfectedIdxs] = nPriUnvacInfected
    end

    # Ru₁: unvaccinated primary recovered (and cross-protected)
    nPriUnvacRecovered =
        unassignedPops .* exp.(-(1.0 / crossProtectDuration) * avgAgeOfGroups)
    unassignedPops -= nPriUnvacRecovered

    sysState[:, colIndexer.priUnvacRecovredIdxs] =
        nPriUnvacRecovered * transpose(serotypeProportions)

    # Su₁: unvaccinated secondary susceptible
    infectedDuration = 1.0 / recoveryRate
    sndSusceptibleDuration =
        avgAgeOfGroups .- (crossProtectDuration + infectedDuration)
    totalFoiOfOtherSTypes = totalPriFoi .- priFois

    nSndSusceptb = zeros(nAgeGroups)
    for ageGroup = 1 : nAgeGroups
        if sndSusceptibleDuration[ageGroup] <= 0.0
            nSndSusceptb[ageGroup] = unassignedPops[ageGroup]
        else
            nSndSusceptb[ageGroup] = unassignedPops[ageGroup] *
                sum(exp.(
                    -totalFoiOfOtherSTypes * sndSusceptibleDuration[ageGroup]
                ))
        end

        if nSndSusceptb[ageGroup] > unassignedPops[ageGroup]
            nSndSusceptb[ageGroup] = unassignedPops[ageGroup]
        end
    end

    unassignedPops -= nSndSusceptb
    sysState[:, colIndexer.sndUnvacSusceptbIdxs] =
        nSndSusceptb * transpose(serotypeProportions)

    # Iu₂: unvaccinated secondary infected
    for ageGroup = 1 : nAgeGroups
        if nSndSusceptb[ageGroup] > 0.0
            # Divide infected individuals into I₁ and I₂
            sysState[ageGroup, colIndexer.sndUnvacInfectedIdxs] =
                sysState[ageGroup, colIndexer.priUnvacInfectedIdxs] / 2.0
            sysState[ageGroup, colIndexer.priUnvacInfectedIdxs] =
                sysState[ageGroup, colIndexer.sndUnvacInfectedIdxs]
        end
    end

    # Ru₂: unvaccinated secondary recovered
    sysState[:, colIndexer.sndUnvacRecoverdIdx] = unassignedPops

    return sysState
end # function



function intepret_ode_time(odeTime::Float64,
                           vars::ModelVars) :: Tuple{Int, Float64}
    odeYear      = Int(floor((odeTime - 1.0) / vars.nDaysPerYear))
    odeYearIdx   = Int(odeYear - vars.nOdeWarmupYears)
    odeDayOfYear = mod(odeTime, vars.nDaysPerYear)
    if odeDayOfYear <= 0.0
        odeDayOfYear = vars.nDaysPerYear
    end
    return (odeYearIdx, odeDayOfYear)
end



function convert_ode_time_to_sim_year(odeTime::Float64,
                                      vars::ModelVars) :: Float64
    odeYear      = floor((odeTime - 1.0) / vars.nDaysPerYear)
    simYear      = odeYear - vars.nOdeWarmupYears
    odeDayOfYear = mod(odeTime - 1.0, vars.nDaysPerYear)
    return simYear + (odeDayOfYear / vars.nDaysPerYear)
end



"""
    pri_fois(sysState::Matrix{Float64},
             vars::ModelVars,
             params::ModelParams
             odeTime::Float64) :: Vector{Float64}
Calculate the primary force of infection of each serotype,
given the current state of the system.
"""
function pri_fois(sysState::Matrix{Float64},
                  vars::ModelVars,
                  params::ModelParams,
                  odeTime::Float64) :: Vector{Float64}

    odeYearIdx, odeDayOfYear = intepret_ode_time(odeTime, vars)
    tranSeasonLength  = vars.nDaysPerYear / vars.nSeasonalTranPeriods
    tranSeasonIdx     = Int(ceil(odeDayOfYear / tranSeasonLength))

    seasonalTranScale = params.seasonalTranScales[tranSeasonIdx]
    serotypeTranRates = params.serotypeTranRates
    transBlockingEfficacy = params.transBlockingEfficacy

    colIndexer =  SIRColumnIndexer(vars.nSerotypes)

    totalInfected =
        sum(sysState[:, colIndexer.priUnvacInfectedIdxs], dims = 1) +
        sum(sysState[:, colIndexer.priVacciInfectedIdxs], dims = 1) +
        sum(sysState[:, colIndexer.priImmunInfectedIdxs], dims = 1) +

        sum(sysState[:, colIndexer.cprUnvacInfectedIdxs], dims = 1) +
        sum(sysState[:, colIndexer.cprVacciInfectedIdxs], dims = 1) +

        sum(sysState[:, colIndexer.sndUnvacInfectedIdxs], dims = 1) +
        sum(sysState[:, colIndexer.sndVacciInfectedIdxs], dims = 1) +
        ( (1.0 - transBlockingEfficacy) *
          sum(sysState[:, colIndexer.sndImmunInfectedIdxs], dims = 1) )

    totalInfected = totalInfected[1, :]

    tranRates = serotypeTranRates * seasonalTranScale
    priFois = (tranRates .* totalInfected) / total_pop_size(sysState)
    return priFois
end



function convert_annual_rate_to_daily_rate(annualRate::Float64,
                                           nDaysPerYear::Float64) :: Float64
    if annualRate > 1.0
        error("Annual rates must not be greater than 1.")
    elseif annualRate == 1.0
        annualRate = 0.999999
    end
    # return log(1.0 / (1.0 - annualRate)) / nDaysPerYear
    return -log(1.0 - annualRate) / nDaysPerYear
end



function convert_annual_rate_to_daily_rate(annualRates::Vector{Float64},
                                           nDaysPerYear::Float64
                                           ) :: Vector{Float64}
    nRates = length(annualRates)
    dailyRates = Vector{Float64}(undef, nRates)
    for i = 1 : nRates
        dailyRates[i] =
            convert_annual_rate_to_daily_rate(annualRates[i], nDaysPerYear)
    end
    return dailyRates
end



function total_pop_size(odeState::Matrix{Float64}) :: Float64
    return sum(odeState[:, 1:end-3])
end



"""
This function is only used by the ODE solver to force all negative solutions to
be rejected.
"""
function is_sys_state_negative(sysState::Matrix{Float64},
                               modelConfig::Tuple{ModelVars, ModelParams},
                               odeTime::Float64) :: Bool
    return any(sysState .< 0.0)
end



function init_sys_state(
        vars::ModelVars,
        params::ModelParams;
        nCalibratingYears::Float64        = 100.0,
        odeAbsTolerance::Float64          = 1e-6,
        odeRelTolerance::Float64          = 1e-4,
        initSerotypeDist::Vector{Float64} = [1.0],
        vaccinateViaAging::Bool           = true) :: Matrix{Float64}

    initPopSize = vars.initPopSize
    if length(vars.annualNatPopGrowthRates) > 0 && vars.nOdeWarmupYears > 0
        minNatPopGrowthRate = minimum(vars.annualNatPopGrowthRates)
        initPopSize = vars.initPopSize /
                      (minNatPopGrowthRate ^ vars.nOdeWarmupYears)
    end

    initVars = deepcopy(vars)
    initVars.nSimYears                 = 0.0
    initVars.simYearToStartVaccination = -1.0
    initVars.noVacBeforeImmigration    = true
    initVars.annualNatPopGrowthRates   = Float64[]
    initVars.annualNetMigRates         = Float64[]
    initVars.nOdeWarmupYears = nCalibratingYears
    initVars.odeAbsTolerance = odeAbsTolerance
    initVars.odeRelTolerance = odeRelTolerance

    firstSysState = zeroth_sys_state(initVars)
    odeEndTime = nCalibratingYears * initVars.nDaysPerYear
    odeTimeSpan = (0.0, odeEndTime)

    odeProblem = ODEProblem(ode_system_vaccinate_within_age_groups,
                            firstSysState,
                            odeTimeSpan,
                            (initVars, params))
    if vaccinateViaAging
        odeProblem = ODEProblem(ode_system_vaccinate_via_aging,
                                firstSysState,
                                odeTimeSpan,
                                (initVars, params))
    end

    odeSolution = solve(odeProblem,
                        OwrenZen5(),
                        abstol = initVars.odeAbsTolerance,
                        reltol = initVars.odeRelTolerance,
                        saveat = [odeEndTime],
                        # alg_hints = [:nonstiff],
                        isoutofdomain = is_sys_state_negative)

    initSysState = odeSolution.u[1]
    Util.zerofy_negative_numbers!(initSysState)

    if length(initSerotypeDist) > 1
        redistribute_serotypes!(initSysState, vars.nSerotypes, initSerotypeDist)
    end

    return initSysState
end # function



function redist_column_proportion!(matrix::Matrix{Float64},
                                   columnIdxs::Vector{Int},
                                   proportions::Vector{Float64})
    colSums = sum(matrix[:, columnIdxs], dims = 2)
    matrix[:, columnIdxs] = colSums .* transpose(proportions)
end # function



function redistribute_serotypes!(odeState::Matrix{Float64},
                                 nSerotypes::Int,
                                 serotypeProportions::Vector{Float64})

    if length(serotypeProportions) != nSerotypes
        throw(ErrorException(
            "the number of serotypes and the length of the distribution vector "
            * "must be the same."
        ))
    end

    colIndexer = SIRColumnIndexer(nSerotypes)
    serotypeProportions = serotypeProportions ./ sum(serotypeProportions)

    redist_column_proportion!(
        odeState, colIndexer.priUnvacInfectedIdxs, serotypeProportions
    )
    redist_column_proportion!(
        odeState, colIndexer.cprUnvacInfectedIdxs, serotypeProportions
    )
    redist_column_proportion!(
        odeState, colIndexer.sndUnvacInfectedIdxs, serotypeProportions
    )
    redist_column_proportion!(
        odeState, colIndexer.priUnvacRecovredIdxs, serotypeProportions
    )
    redist_column_proportion!(
        odeState, colIndexer.sndUnvacSusceptbIdxs, serotypeProportions
    )
end



function solve_odes(vars::ModelVars,
                    params::ModelParams,
                    odeInitState::Matrix{Float64},
                    vaccinateViaAging::Bool) :: ODESolution
    simStartDay = vars.nOdeWarmupYears * vars.nDaysPerYear +
                  1.0 - vars.nDaysReportingLag
    simEndDay = ceil(vars.nSimYears * vars.nDaysPerYear) + simStartDay - 1.0
    simEndDay = simEndDay < simStartDay ? simStartDay : simEndDay
    daysToSave = [t for t = simStartDay : 1.0 : simEndDay]
    odeStartDay = simStartDay > 0.0 ? 0.0 : simStartDay
    odeTimeSpan = (odeStartDay, simEndDay)

    odeProblem = ODEProblem(ode_system_vaccinate_within_age_groups,
                            odeInitState,
                            odeTimeSpan,
                            (vars, params))
    if vaccinateViaAging
        odeProblem = ODEProblem(ode_system_vaccinate_via_aging,
                                odeInitState,
                                odeTimeSpan,
                                (vars, params))
    end

    odeSolution = solve(odeProblem,
                        # OwrenZen3(),
                        # OwrenZen5(),
                        # TRBDF2(),
                        Vern7(),
                        abstol = vars.odeAbsTolerance,
                        reltol = vars.odeRelTolerance,
                        saveat = daysToSave,
                        # alg_hints = [:nonstiff],
                        isoutofdomain = is_sys_state_negative)
    return odeSolution
end # function



function extract_ode_class(odeSolution::ODESolution,
                           odeClassIdx::Union{Int, UnitRange, Vector{Int}},
                           recordingTimeStep::Int) :: Matrix{Float64}
    nOdeTimePoints = length(odeSolution.u)
    nAgeGroups, nOdeClasses = size(odeSolution.u[1])
    nRecordedTimes = floor(Int, nOdeTimePoints / recordingTimeStep)

    result = Matrix{Float64}(undef, nAgeGroups, nRecordedTimes)
    recordedTime = 0
    for odeTime = 1 : recordingTimeStep : nOdeTimePoints
        recordedTime += 1
        result[:, recordedTime] =
            sum(odeSolution.u[odeTime][:, odeClassIdx], dims = 2)
    end
    return result
end






end  # module Odes
